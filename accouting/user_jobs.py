#!/usr/bin/env python
import sys
import time

def main():
  
  user=sys.argv[1]
  acct="/scratch/projects/tacc_stats/accounting/tacc_jobs_completed"

  for line in open(acct,"r"):
    line=line[:-1]
    try:
      (jobid,uid,proj,yn,start,end,sub,queue,unkn,name,status,nodes,cores)=line.split(":")
    except:
      continue
    if user==uid:
      print jobid, uid, time.strftime('%Y-%m-%d %H:%M:%S',
      time.localtime(float(end))), nodes

if __name__=='__main__':
  main()
