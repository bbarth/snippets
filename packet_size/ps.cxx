#include <iostream>
#include "stdio.h"
#include "stdlib.h"

#define MPICH_SKIP_MPICXX 1
#ifdef __cplusplus
extern "C" {
  #include <mpi.h>
}
#endif

#define MCW MPI_COMM_WORLD
#define N 27

int fib[27]={0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393};

int main(int argc, char* argv[])
{
  int np, i_am;
  char mach_name[MPI_MAX_PROCESSOR_NAME];
  int mach_len=MPI_MAX_PROCESSOR_NAME-1;

  MPI_Init (&argc,&argv);
  MPI_Comm_size (MPI_COMM_WORLD, &np);
  MPI_Comm_rank (MPI_COMM_WORLD, &i_am);

  if (np % 2 != 0)
  {
    MPI_Abort(MCW,1);
  }

  char *sbuf, *rbuf;
  sbuf=(char* )malloc(sizeof(char)*fib[26]);
  rbuf=(char* )malloc(sizeof(char)*fib[26]);
  int neigh=-1;
  int iter_max=1;
  for (int iter=0; iter<iter_max; ++iter)
  {
    for (int i=0; i < fib[26]; ++i)
    {
      sbuf[i]=i%256;
      rbuf[i]=0;
    }

    for (int i=0; i < N; ++i)
    {
// evens send, odds recv
      
      if (i_am % 2) //even
      {
        neigh=(i_am!=np-1)?i_am+1:0;
        int ierr=MPI_Ssend(&sbuf[0],fib[i],MPI_BYTE,neigh,123456,MCW);
      }
      else // odd
      {
        MPI_Status stat;
        neigh=(i_am!=0)?i_am-1:np-1;
        int ierr=MPI_Recv(&rbuf[0],fib[i],MPI_BYTE,neigh,123456,MCW,&stat);
      }
    }
  }
    
  MPI_Finalize();

  return 0;
}
