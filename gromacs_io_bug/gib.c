#include <stdio.h>
#include <sys/types.h>

int main(int argc,char *argv[]){

  FILE *fp;
  int i,j,ni,nj;
  int n=7000;
#ifdef HAVE_OFFT
  off_t offset;
#else
#warning "using long int for offsets"
  long int offset;
#endif
  fp = fopen("in2.txt","a+");

  ni=atoi(argv[1]);
  nj=atoi(argv[2]);
  printf("size of the problem:%d %d\n\n",ni,nj);
  //printf("\n size of int %jd\n",sizeof(int));

  for(i=0; i<ni; i++){
    printf("step %d\n",i);
#ifdef HAVE_OFFT
    offset = ftello(fp);
    printf("off_t offset before write to file: %jd\n", offset);
#else
    offset = ftell(fp);
    printf("offset before write to file: %ld\n", offset);
#endif

    // write something to fp. The problem occurs often as more stuff written to fp
    for (j=0;j<nj;j++){
      fprintf(fp,"%d\n",n);
    }

    // added a flush to be consistent with the way of Gromacs
    fflush(fp);
#ifdef HAVE_OFFT
    offset = ftello(fp);
    printf("off_t offset after write to file: %jd\n", offset);

    fseeko(fp,0,SEEK_END);
    offset = ftello(fp);
    printf("off_t offset after seek: %jd\n\n", offset);
#else
    offset = ftell(fp);
    printf("offset after write to file: %ld\n", offset);

    fseek(fp,0,SEEK_END);
    offset = ftell(fp);
    printf("offset after seek: %ld\n\n", offset);
#endif
  }
  fclose(fp);
  return 0;
}
