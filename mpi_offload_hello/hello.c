#include <stdio.h>
#include <unistd.h>
#include <mpi.h>
#include <omp.h>

int main(int argc, char **argv)
{

  int np, i_am;
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &i_am);

  double a[1024];
  int i=0.;
  for(i=0; i < 1024; ++i)
    a[i]=(double) ((i%64)+i_am);

  char host[256];
  double sum=0.;
#pragma offload target(mic:0) // Start an offload region, print out the hostname
  {
    gethostname(host,256);
    printf("%s\n",host);
    fflush(0);
#pragma omp parallel // Start OpenMP paralle region, get thread number
    {
      int tid=omp_get_thread_num();
      printf("%s: %d of %d\n",host, tid, omp_get_num_threads());
      fflush(0);
#pragma omp for reduction(+:sum) // Sum up the local values of a[]
      for (i=0; i < 1024; ++i)
	sum+=a[i]+tid;
    }
  }

  double gsum=0.;

  // Sum up the values of a[] globally
  MPI_Allreduce(&sum, &gsum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  int len=0;
  char name[MPI_MAX_PROCESSOR_NAME];
  
  MPI_Get_processor_name(name,&len);

  printf("Hello from %d of %d on %s, our total is %8.3g of %8.3g\n",
	 i_am,np,name,sum,gsum);

  MPI_Finalize();
      
  return 0;
}
