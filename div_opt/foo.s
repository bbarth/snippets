# mark_description "Intel(R) C Intel(R) 64 Compiler XE for applications running on Intel(R) 64, Version 13.1.0.146 Build 2013012";
# mark_description "1";
# mark_description "-std=c99 -O3 -xHost -S";
	.file "foo.c"
	.text
..TXTST0:
# -- Begin  main
# mark_begin;
       .align    16,0x90
	.globl main
main:
# parameter 1: %edi
# parameter 2: %rsi
..B1.1:                         # Preds ..B1.0
..___tag_value_main.1:                                          #5.1
        pushq     %rbp                                          #5.1
..___tag_value_main.3:                                          #
        movq      %rsp, %rbp                                    #5.1
..___tag_value_main.4:                                          #
        andq      $-128, %rsp                                   #5.1
        pushq     %r12                                          #5.1
        pushq     %r15                                          #5.1
        pushq     %rbx                                          #5.1
        subq      $104, %rsp                                    #5.1
..___tag_value_main.6:                                          #
        movl      %edi, %r12d                                   #5.1
        movl      $3, %edi                                      #5.1
        movq      %rsi, %r15                                    #5.1
..___tag_value_main.9:                                          #5.1
        call      __intel_new_proc_init_G                       #5.1
..___tag_value_main.10:                                         #
                                # LOE r13 r14 r15 r12d
..B1.11:                        # Preds ..B1.1
        vstmxcsr  (%rsp)                                        #5.1
        xorl      %ebx, %ebx                                    #6.12
        orl       $32832, (%rsp)                                #5.1
        vldmxcsr  (%rsp)                                        #5.1
        cmpl      $1, %r12d                                     #7.11
        jle       ..B1.4        # Prob 78%                      #7.11
                                # LOE r13 r14 r15 ebx r12d
..B1.2:                         # Preds ..B1.11
        movq      8(%r15), %rcx                                 #8.11
        call      __intel_sse4_atol                             #8.11
                                # LOE rax r13 r14 r15 r12d
..B1.3:                         # Preds ..B1.2
        movl      %eax, %ebx                                    #8.11
                                # LOE r13 r14 r15 ebx r12d
..B1.4:                         # Preds ..B1.3 ..B1.11
        movl      $1, %edx                                      #12.11
        cmpl      $2, %r12d                                     #13.13
        jle       ..B1.7        # Prob 78%                      #13.13
                                # LOE r13 r14 r15 edx ebx
..B1.5:                         # Preds ..B1.4
        xorl      %esi, %esi                                    #14.11
        movq      16(%r15), %rdi                                #14.11
        vzeroupper                                              #14.11
        call      strtod                                        #14.11
                                # LOE r13 r14 ebx xmm0
..B1.6:                         # Preds ..B1.5
        vcvttsd2si %xmm0, %edx                                  #14.11
                                # LOE r13 r14 edx ebx
..B1.7:                         # Preds ..B1.6 ..B1.4
        vcvtsi2sd %edx, %xmm1, %xmm1                            #21.11
        vmovsd    .L_2il0floatpacket.7(%rip), %xmm12            #18.22
        movl      $4, %edx                                      #16.3
        vdivsd    %xmm1, %xmm12, %xmm13                         #21.3
        vmovd     %edx, %xmm5                                   #16.3
        movl      $.L_2__STRING.0, %edi                         #31.3
        vmovdqu   .L_2il0floatpacket.6(%rip), %xmm6             #16.3
        movl      $1, %eax                                      #31.3
        vpshufd   $0, %xmm5, %xmm7                              #16.3
        vpaddd    %xmm7, %xmm6, %xmm8                           #21.3
        vcvtdq2pd %xmm6, %ymm3                                  #18.22
        vcvtdq2pd %xmm8, %ymm9                                  #18.22
        vmovddup  %xmm1, %xmm2                                  #21.11
        vmovsd    %xmm13, 64(%rsp)                              #21.3
        movslq    %ebx, %rbx                                    #31.3
        vinsertf128 $1, %xmm2, %ymm2, %ymm10                    #21.11
        vdivpd    %ymm10, %ymm3, %ymm4                          #21.3
        vdivpd    %ymm10, %ymm9, %ymm11                         #21.3
        vmovupd   %ymm4, (%rsp)                                 #21.3
        vmovupd   %ymm11, 32(%rsp)                              #21.3
        vmovsd    (%rsp,%rbx,8), %xmm0                          #31.3
        vzeroupper                                              #31.3
..___tag_value_main.11:                                         #31.3
        call      printf                                        #31.3
..___tag_value_main.12:                                         #
                                # LOE r13 r14
..B1.8:                         # Preds ..B1.7
        xorl      %eax, %eax                                    #33.9
        addq      $104, %rsp                                    #33.9
..___tag_value_main.13:                                         #33.9
        popq      %rbx                                          #33.9
..___tag_value_main.14:                                         #33.9
        popq      %r15                                          #33.9
..___tag_value_main.15:                                         #33.9
        popq      %r12                                          #33.9
        movq      %rbp, %rsp                                    #33.9
        popq      %rbp                                          #33.9
..___tag_value_main.16:                                         #
        ret                                                     #33.9
        .align    16,0x90
..___tag_value_main.18:                                         #
                                # LOE
# mark_end;
	.type	main,@function
	.size	main,.-main
	.data
# -- End  main
	.section .rodata, "a"
	.align 16
	.align 16
.L_2il0floatpacket.6:
	.long	0x00000000,0x00000001,0x00000002,0x00000003
	.type	.L_2il0floatpacket.6,@object
	.size	.L_2il0floatpacket.6,16
	.align 8
.L_2il0floatpacket.7:
	.long	0x00000000,0x40200000
	.type	.L_2il0floatpacket.7,@object
	.size	.L_2il0floatpacket.7,8
	.section .rodata.str1.4, "aMS",@progbits,1
	.align 4
	.align 4
.L_2__STRING.0:
	.byte	37
	.byte	103
	.byte	10
	.byte	0
	.type	.L_2__STRING.0,@object
	.size	.L_2__STRING.0,4
	.data
	.section .note.GNU-stack, ""
// -- Begin DWARF2 SEGMENT .eh_frame
	.section .eh_frame,"a",@progbits
.eh_frame_seg:
	.align 8
	.4byte 0x00000014
	.8byte 0x7801000100000000
	.8byte 0x0000019008070c10
	.4byte 0x00000000
	.4byte 0x0000007c
	.4byte 0x0000001c
	.8byte ..___tag_value_main.1
	.8byte ..___tag_value_main.18-..___tag_value_main.1
	.byte 0x04
	.4byte ..___tag_value_main.3-..___tag_value_main.1
	.2byte 0x100e
	.byte 0x04
	.4byte ..___tag_value_main.4-..___tag_value_main.3
	.4byte 0x8610060c
	.2byte 0x0402
	.4byte ..___tag_value_main.6-..___tag_value_main.4
	.8byte 0xff800d1c380e0310
	.8byte 0xffffffe80d1affff
	.8byte 0x800d1c380e0c1022
	.8byte 0xfffff80d1affffff
	.8byte 0x0d1c380e0f1022ff
	.8byte 0xfff00d1affffff80
	.4byte 0x0422ffff
	.4byte ..___tag_value_main.13-..___tag_value_main.6
	.2byte 0x04c3
	.4byte ..___tag_value_main.14-..___tag_value_main.13
	.2byte 0x04cf
	.4byte ..___tag_value_main.15-..___tag_value_main.14
	.2byte 0x04cc
	.4byte ..___tag_value_main.16-..___tag_value_main.15
	.8byte 0x00000000c608070c
# End
