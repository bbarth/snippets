#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  int index=0;
  if(argc>1)
    index=atoi(argv[1]);

  double a[9];

  int det = 1.;
  if(argc > 2)
    det = atof(argv[2]);

  for (int i=0; i < 9 ; ++i)
    {
      a[i]=((double) i);
    }
  
  a[0] /= det;
  a[1] /= det;
  a[2] /= det;
  a[3] /= det;
  a[4] /= det;
  a[5] /= det;
  a[6] /= det;
  a[7] /= det;
  a[8] /= det;
  
  printf("%g\n",a[index]);

  return(0);
}
