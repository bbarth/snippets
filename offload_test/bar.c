#include <stdlib.h>

int bar(int n)
{
  double *a, *b;

  a=malloc(n*sizeof(double));
  b=malloc(n*sizeof(double));

  
  for (int i=0; i < n; ++i)
    {
      a[i]=1./((double)(i));
      b[i]=(double)(i);
    }

#pragma offload target(mic) in(a:length(n)) inout(b:length(n))
#pragma omp parallel for 
  for (int i=0; i < n; ++i)
    {
      b[i]*=a[i];
    }

  int sum=0;

  for (int i=0; i < n; ++i)
    {
      sum += (int)(b[i]);
      sum = sum % 128;
    }

  return(sum);
}
