#include <stdio.h>
#include <stdlib.h>

int bar(int);

int main(int argc, char **argv)
{
  if (argc<2)
    exit(1);
  int a=atoi(argv[1]);
  int b=bar(a);
  printf("%d\n",b);
  return(b);
}
