#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>

int main(int argc, char **argv)
{

  char *dir=".";

  if (argc > 1)
    dir=argv[1];

  DIR *d = opendir(dir);

  if (d != NULL)
    {
      struct dirent *de;
      while ((de=readdir(d)) != NULL)
	{
	  printf("%lu %s\n",de->d_ino, de->d_name);
	}

      closedir(d);
    }
  
}
