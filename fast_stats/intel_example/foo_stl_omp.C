#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <math.h>

#include <vector>
using namespace std;

int main(int argc, char **argv)
{
  int n=atoi(argv[1]);

  size_t N=1024*1024*n;

  printf("%16zd\n",N);

  vector<double> d(N);

  

  printf("Initialize\n"); fflush(stdout);

  for (size_t i=0; i<N; ++i)
    d[i]=(double)(i);

  struct timeval ts, te;

  printf("Average\n"); fflush(stdout);
  gettimeofday(&ts,NULL);

  double sum=0.;
  for (size_t i=0; i<N; ++i)
    sum+=d[i];
  
  sum /= (double)(N);

  gettimeofday(&te,NULL);

  double ds=(double)(ts.tv_sec) + 1.e-6*(double)(ts.tv_usec);
  double de=(double)(te.tv_sec) + 1.e-6*(double)(te.tv_usec);

  double time=(de-ds);

  printf("%15.7g %15.7g\n",sum,time);

  double sigma=0.;

  printf("Std. Dev.\n"); fflush(stdout);
  gettimeofday(&ts,NULL);

#pragma omp parallel for reduction(+:sigma)
  for (size_t i=0; i<N; ++i)
    sigma+=(d[i]-sum)*(d[i]-sum);

  sigma /= (double)(N);
  sigma=sqrt(sigma);
  
  gettimeofday(&te,NULL);

  ds=(double)(ts.tv_sec) + 1.e-6*(double)(ts.tv_usec);
  de=(double)(te.tv_sec) + 1.e-6*(double)(te.tv_usec);

  time=(de-ds);

  printf("%15.7g %15.7g\n",sigma,time);

  return(0);
}
