program foo_ftnalloc

integer*8                         :: n, nm, i, ts, te, cr
real*8, dimension(:), allocatable :: x
real*8                            :: sum, sigma
character(len=32) :: arg

call get_command_argument(1, arg)
read( arg, '(i10)' )  n

nm=n*1024*1024
write (0,'(I16)') nm

allocate(x(nm))

write (0,*) 'Initializing'
do i=1, nm
  x(i) = real(i-1,8)
enddo

write (0,*) 'Average'
call system_clock(ts,cr)
sum = 0.
do i=1, nm
  sum = sum + x(i)
enddo

sum = sum / real(nm,8)
call system_clock(te,cr)

write (0,'(1pg15.7,1pg15.7)') sum, real((te-ts),8)/real(cr,8)

write (0,*) 'Std. Dev.'
ts=0
te=0

call system_clock(ts,cr)
sigma=0.

!$omp parallel do reduction(+:sigma)
do i=1,nm
  sigma=sigma+((x(i)-sum)**2)
enddo

sigma=sigma/real(nm,8)
sigma=sqrt(sigma)
call system_clock(te,cr)

write (0,'(1pg15.7,1pg15.7)') sigma, real((te-ts),8)/real(cr,8)

end program
