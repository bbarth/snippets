#!/usr/bin/env python3
# -*- python -*-
import os, sys, re
import h5py
import numpy as np

def main():

  print('This thing is on!')

  with h5py.File("a.h5",'w') as f:
    a=np.random.random((5,5))
    dset = f.create_dataset("a",shape=a.shape,data=a)

if ( __name__ == '__main__'):
  main()

