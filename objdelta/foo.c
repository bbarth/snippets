#include <stdio.h>
#include <math.h>

//#define N (1024*1024*1024)
//#define N (1024*1024)
#define N (10*1024*1024)
size_t some_function(double q)
{
  double BNM[N]={1.};
  double mx=0.;

  for (size_t i=0; i<N; i++)
    {
      double I=(double) i;
      BNM[i]=0.;
      if (i%7) BNM[i]=q;
      mx=fmax(I,mx);
    }
  
  return((size_t) mx);
}

int main(int argc, char* argv[])
{
  unsigned long long bits=0x7ff0dead7ff0beefULL;
  size_t n=N;
  double db=*((double *) &bits);
  double ASDF[N]={db};

  ASDF[0]=1.0;
  ASDF[N-1]=ASDF[0]+10.;

  for(size_t i=0; i<N; i++)
    ASDF[i]=(double) (i);
  
  printf("Hello World!,%8.3g,%8.3g, %zd, %zd\n",ASDF[0],ASDF[N-1], some_function(ASDF[N/2]),n); 

  return (ASDF[N-1]+ASDF[0]);
}
