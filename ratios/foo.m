clear all
close all

maxcases=10;
n=2.^[0:5];
ep=0.10;
t1=1.;

r=floor(rand(size(n))*10)+2;

y=cell(size(n));
for j=1:length(r)
  y{j}=[];
  for i=1:r(j)
    y{j}(i)=t1/n(j)+ep/n(j)*(1-0.5*rand(1));
  end
end

figure(1)
hold on
for i=1:length(n)
  plot(n(i)*ones(length(y{i})),y{i},'x')
end

figure(2)
hold on
m=mean(y{1});
miny1=min(y{1});
for i=1:length(n)
  s=m*ones(size(y{i}))./y{i};
  sm=miny1*ones(size(y{i}))./y{i};
  plot(n(i)*ones(length(s)),s,'x')
  plot(n(i)*ones(length(sm)),sm,'o')
end

