#include <stdio.h>
#include "mpi.h"

int needs_func_ptr( int (*fun)(int arg), int a)
{
  return fun(a);
}  

//int foo(int a)
//{
//  printf("in foo\t");
//  int bar( int c)
//  {
//    printf("in bar\n");
//    return c*a;
//  }
//  return a*bar(a);
//}

int foo(int a)
{

  return(100*a);
  
}

int main(int argc, char* argv[])
{
  int bar(int b)
  {
    return 50*b;
  }
  MPI_Init (&argc,&argv);
  printf("Hello World!, main: %d\n", needs_func_ptr(bar,10)); 

  MPI_Finalize();
  return 0;
}
