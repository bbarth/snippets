program main

implicit none

integer, parameter :: n = 100000000
integer i
integer loc
integer*8 ts,te

real*8, dimension(:), allocatable :: a,b,c
real*8 rand, rv

allocate(a(n),b(n),c(n))

do i=1,n
   a(i)=real(i)
   b(i)=sqrt(real(i))
   c(i)=0.
end do

call system_clock(ts)

!$omp parallel workshare
c=a+b
!$omp end parallel workshare

call system_clock(te)

print *, te-ts

!rv=rand()
call random_number(rv)
loc=int(real(n)*rv)

print*, c(loc)

call system_clock(ts)

!$omp parallel do
do i=1,n
   c(i)=a(i)+b(i)
end do
!$omp end parallel do

call system_clock(te)

print *, te-ts

!rv=rand()
call random_number(rv)
loc=int(real(n)*rv)

print*, c(loc)



stop
end program main
