#include <fstream>
#include <iomanip>

using namespace std;

int main()
{
  ofstream fs;
  fs.open("larry.out");

  fs << setprecision(25) << 1.000001 << endl;

  fs.close();

  exit(0);
}
