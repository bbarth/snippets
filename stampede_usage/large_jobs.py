#!/usr/bin/env python

import sys
import re

def passwd_uid_to_username(filename):
  #    fields='username:password:uid:gid:fullname:homedir:shell'.split(':')
  FILE = open(filename, "r")
  passwdfile=[y.strip() for y in FILE.readlines()]
  FILE.close()
  return dict([(m[2],m[0]) for m in [l.split(':') for l in passwdfile]])

def passwd_username_to_fullname(filename):
  FILE = open(filename, "r")
  passwdfile=[y.strip() for y in FILE.readlines()]
  FILE.close()
  return dict([(m[0],m[4]) for m in [l.split(':') for l in passwdfile]])

def main():
  usernames=passwd_uid_to_username('/etc/passwd')
  for l in open(sys.argv[1],"r"):
    l=l[:-1]
    try:
      (jid,uid,prj,yn,start,stop,qt,queue,unkn,name,status,nodes,cores) = l.split(':')
    except ValueError:
      continue

    un=usernames[uid]

    if ((int(nodes) > 256) and
        (int(start) >= 1375333200) and (int(stop) < 1406869200) and
        un != 'minyard' and un != 'viennej' and
        un != 'karl' and un != 'carlos'):
      print usernames[uid], nodes
        

if __name__ == "__main__":
  main()
