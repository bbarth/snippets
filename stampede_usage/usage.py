#!/usr/bin/env python

import sys
import re

def passwd_uid_to_username(filename):
  #    fields='username:password:uid:gid:fullname:homedir:shell'.split(':')
  FILE = open(filename, "r")
  passwdfile=[y.strip() for y in FILE.readlines()]
  FILE.close()
  return dict([(m[2],m[0]) for m in [l.split(':') for l in passwdfile]])

def passwd_username_to_fullname(filename):
  FILE = open(filename, "r")
  passwdfile=[y.strip() for y in FILE.readlines()]
  FILE.close()
  return dict([(m[0],m[4]) for m in [l.split(':') for l in passwdfile]])

def main():
  f=open(sys.argv[1],"r")
  
  pwdict=passwd_uid_to_username("/etc/passwd")
  pwnamedict=passwd_username_to_fullname("/etc/passwd")
  cnt={}
  prj={}
  for l in f.readlines():
    mo=re.search(sys.argv[2],l.rstrip())
    if mo:
      r=l.rstrip().split(':')
      r[1]=pwdict[r[1]]
      cnt[r[1]]=cnt.get(r[1],0) + 1
      if not r[1] in prj:
        prj[r[1]] = set()
      prj[r[1]].add(r[2])
      
  for w in sorted(cnt, key=cnt.get):
    if not 'STAR-Intel' in prj[w] \
          and not 'A-ccsc' in prj[w] \
          and not 'StampedeIntel' in prj[w]:
      print ' '.join([w, pwnamedict[w], str(cnt[w])]+ list(prj[w]))
    


  


if __name__ == "__main__":
  main()
