#!/usr/bin/env python

import sys
import re

def passwd_uid_to_username(filename):
  #    fields='username:password:uid:gid:fullname:homedir:shell'.split(':')
  FILE = open(filename, "r")
  passwdfile=[y.strip() for y in FILE.readlines()]
  FILE.close()
  return dict([(m[2],m[0]) for m in [l.split(':') for l in passwdfile]])

def passwd_username_to_fullname(filename):
  FILE = open(filename, "r")
  passwdfile=[y.strip() for y in FILE.readlines()]
  FILE.close()
  return dict([(m[0],m[4]) for m in [l.split(':') for l in passwdfile]])

def main():
  project=sys.argv[1]

  sus=0.
  micsus=0.
  for l in open(sys.argv[2],"r"):
    l=l[:-1]
    try:
      (jid,uid,prj,yn,start,stop,qt,queue,unkn,name,status,nodes,cores) = l.split(':')
    except ValueError:
      continue
    if ((prj==project) and
        (int(start) >= 1375333200) and (int(stop) < 1406869200)):
      su=float((int(stop)-int(start))*int(nodes))*16./3600.
      sus+=su
      if queue=="normal-mic" or queue=="normal-2mic":
        micsus+=su
        

  print project,sus,micsus


  


if __name__ == "__main__":
  main()
