#!/usr/bin/env python

import sys
import re
from collections import defaultdict

def passwd_uid_to_username(filename):
  #    fields='username:password:uid:gid:fullname:homedir:shell'.split(':')
  FILE = open(filename, "r")
  passwdfile=[y.strip() for y in FILE.readlines()]
  FILE.close()
  return dict([(m[2],m[0]) for m in [l.split(':') for l in passwdfile]])

def passwd_username_to_fullname(filename):
  FILE = open(filename, "r")
  passwdfile=[y.strip() for y in FILE.readlines()]
  FILE.close()
  return dict([(m[0],m[4]) for m in [l.split(':') for l in passwdfile]])

def main():
  f=open(sys.argv[1],"r")
  
  pwdict=passwd_uid_to_username("/etc/passwd")
  pwnamedict=passwd_username_to_fullname("/etc/passwd")
#  cnt={}
  prj={}
  sus=defaultdict(float)
  for l in f.readlines():
    mo=True #re.search(sys.argv[2],l.rstrip())
    if mo:
      l=l[:-1]
      r=l.split(':')
      try:
        (jid,uid,project,yn,start,stop,qt,queue,unkn,name,status,nodes,cores)=r
      except:
        continue
           
      uname=pwdict[uid]
      su=float((int(stop)-int(start))*int(nodes))*16./3600.
      sus[uname]+=su
      if not uname in prj:
        prj[uname] = set()
      prj[uname].add(project)
      
  for w in sorted(sus, key=sus.get, reverse=True):
    if not 'STAR-Intel' in prj[w] \
          and not 'A-ccsc' in prj[w] \
          and not 'StampedeIntel' in prj[w]\
          and not 'default' in prj[w]:
      print ' '.join([w, pwnamedict[w], str(sus[w])] + list(prj[w]))
    


  


if __name__ == "__main__":
  main()
