#include <mpi.h>
#include <iostream>
 
int main( int argc, char *argv[] )
{
  int rank, nproc;
 
  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &nproc );
  
  const int count=31999;  
  
  int sendbuf[count], recvbuf[count];
  for(int i=0; i<count; i++) {
    sendbuf[i] = 0; 
    recvbuf[i] = 0; 
  }
 
  if(rank==0) 
    std::cout<<"MPI_REDUCE with "<<count<<" MPI_INTs"<<std::endl;    
  
  MPI_Reduce(sendbuf, recvbuf, count, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  
  if(rank==0)
    for(int i=0; i<count; i++)
      if(recvbuf[i]!=0)
        std::cout<<"After reduce, i = "<<i<<" recvbuf = "<<recvbuf[i]<<std::endl;
 
  MPI_Finalize();
  return 0;

}
