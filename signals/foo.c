#include <signal.h>
#include <unistd.h>
#include <stdio.h>

FILE *f;
     
void termination_handler (int signum)
{
  while (1)
    {
      fprintf(f,"Caught %d\n",signum);
      fflush(f);
      sleep(1);
    }
}     
int main (void)
{

  f=fopen("testfile","w");
  struct sigaction new_action, old_action;
     
  /* Set up the structure to specify the new action. */
  new_action.sa_handler = termination_handler;
  sigemptyset (&new_action.sa_mask);
  new_action.sa_flags = 0;
     
  sigaction (SIGTERM, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN)
    sigaction (SIGTERM, &new_action, NULL);

  while(1)
    {
      fprintf(f,"In main loop, sleeping 10 seconds\n");
      fflush(f);
      sleep(10);
    }
  exit(0);
}
