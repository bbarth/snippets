#!/usr/bin/env python

import matplotlib.pyplot as p
import numpy
import scipy


x=numpy.arange(0.0,1.0,.01)
y=scipy.sin(scipy.pi*x)

fig=p.figure()
ax=fig.gca()
ax.plot(x,y)
ax.set_title('sin(pi x)')
ax.set_xlabel('x')
ax.set_ylabel('sin(pi x)')
fname='foo.png'
fig.savefig(fname)



