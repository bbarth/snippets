#ifndef LOCKFILEHANDLER_H_
#define LOCKFILEHANDLER_H_

#include <ctime>
#include <string>

/**
 * Manage lock files, even over network filesystems.
 *
 * Handling locks through this interface should be safe from deadlocks and race
 * conditions, even when competing with separate processes on separate hosts
 * sharing the same filesystem.  Stale locks are deleted robustly (though it is
 * up to the client to ensure the integrity of the locked resources).
 *
 * This class is not thread-safe (each thread must construct its own instance).
 */
class LockfileHandler {
 public:
  /**
   * Construct a new handler for the specified file.  No attempt is made to
   * acquire the lock.  Using an absolute path for "filename" is recommended.
   *
   * @param filename The name of the lock file to handle
   */
  LockfileHandler(const std::string& filename);

  /**
   * Attempt to acquire the lock.  Stale locks will be deleted.  The deletion of
   * stale locks is made safe by first acquiring a lock file (named
   * filename+".dellock") granting permission to delete the stale lock.  This
   * method may not be called if the lock is currently held by this handler.
   * To avoid checking for stale locks, set "t_stale" to a negative value.
   *
   * @param t_wait Number of seconds to wait between attempts
   * @param t_stale Age of a lock (in seconds) that will be considered stale
   * @param max_tries Maximum number of attempts before returning
   * @return True if the lock was successfully acquired; othrwise false
   */
  bool Acquire(double t_wait, double t_stale, int max_tries);

  /**
   * Release the lock held by this handler.  This method may not be called if
   * this handler does not currently hold the lock.
   */
  void Release();

  /**
   * Query whether or not this handler currently holds its lock.
   *
   * @return True if the lock is currently held; otherwise false
   */
  bool HasLock() const;

  /**
   * Return the name of the lock file managed by this handler.
   *
   * @return Name of lock file being handled.
   */
  const std::string& get_filename() const { return filename_; }

 private:
  const std::string filename_;
  int fd_;

  /**
   * Construct a string unique to this process.  An example would be a
   * concatenation of the hostname and the PID.
   *
   * @return A string unique to this process
   */
  std::string GetProcessLabel() const;

  /**
   * Exclusively create a file in a way compatible with NFSv2.
   *
   * @param filename Name of file to create
   * @param flags Access mode flag (see open(2))
   * @return File descriptor of created file, or -1 on error
   */
  static int OpenCreatExclNfs2(const std::string& filename, int flags);

  /**
   * Return the age of a file in seconds, if it exists, or 0.0 otherwise.
   * Age is defined as the difference between the current time and the file's
   * mtime.
   *
   * @param filename Name of file whose age is to be measured
   * @return The age of the file, in seconds, or 0.0 if it does not exist
   */
  static double ComputeAge(const std::string& filename);

  /**
   * Attempt to safely delete a stale lock.  Permission for deletion is granted
   * by a lock file named filename+".dellock".  Only one attempt is made to
   * acquire this delete-lock; if the delete-lock is already held, assume the
   * original stale lock is in the process of being deleted.  Delete-locks are
   * considered stale after the same "t_stale" as the original lock, and their
   * deletion is handled recursively.
   *
   * @param filename Path to lock file to delete
   * @param t_stale Age of lock (in seconds) that will be considered stale
   * @return True if the lock was deleted or was not found to be stale
   */
  static bool DeleteStaleLock(const std::string& filename, double t_stale);

  /**
   * Convert a double-precision positive time interval in seconds to a "struct
   * timespec" with nanosecond precision.
   *
   * @param secs The time interval, in seconds
   * @param ts The "struct timespec" to write the interval to
   */
  static void DoubleToTimespec(double secs, struct timespec* ts);
};

#endif  // LOCKFILEHANDLER_H_

