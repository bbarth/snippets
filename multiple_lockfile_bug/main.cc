#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <string>

#include "LockfileHandler.h"

int main(int argc, char** argv) {
  // Parse command line arguments
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <path_to_work_dir>" << std::endl;
    return EXIT_FAILURE;
  }
  std::string dir(argv[1]);
  std::string lockfile(dir + "/Lockfile.lck");

  // Each rank will try to acquire a lock n_iter times
  const int n_iter = 2;

  LockfileHandler lock(lockfile);
  for (int i=0; i<n_iter; ++i) {
    // Wait up to 10 min for a lock; consider locks stale after 5 min
    const bool locked = lock.Acquire(1.0, 300.0, 600);
    if (locked) {
      sleep(2);
      lock.Release();
    } else {
      std::cerr << "Failed to acquire lock" << std::endl;
    }
    sleep(1);
  }

  return EXIT_SUCCESS;
}

