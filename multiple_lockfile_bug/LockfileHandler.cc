#include "LockfileHandler.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cerrno>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <sstream>

#define ASSERT(a,m) \
  if (!(a)) { \
    std::cerr << "Assertion failure: " << m << std::endl; \
    std::cerr << "  " << __FILE__ << ":" << __LINE__ << std::endl; \
    abort(); \
  }

#define REQUIRE(a,m) \
  if (!(a)) { \
    std::cerr << "Requirement failure: " << m << std::endl; \
    std::cerr << "  " << __FILE__ << ":" << __LINE__ << std::endl; \
    abort(); \
  }

LockfileHandler::LockfileHandler(const std::string& filename)
    : filename_(filename), fd_(-1) { }

bool LockfileHandler::Acquire(const double t_wait, const double t_stale,
                              const int max_tries) {
  REQUIRE(!HasLock(),
          "Cannot acquire lock on '" + filename_ + "' - lock already held!\n"
          "This indicates a logic error in the code using this lock.");

  // Create lockfiles with restrictive permissions
  const mode_t mode = 0;
  //const int flags = O_WRONLY|O_SYNC|O_CREAT|O_EXCL;

  // Convert wait time to the proper type
  struct timespec ts_wait;
  DoubleToTimespec(t_wait, &ts_wait);

  // Determine age of current lock (if any), and delete it if it is stale
  double age = 0.0;
  if (t_stale >= 0.0) {
    age = ComputeAge(filename_);
    if (age > t_stale) {
      DeleteStaleLock(filename_, t_stale);
    }
  }

  // Try to create the lockfile
  //int fd = open(filename_.c_str(), flags, mode);
  int fd = OpenCreatExclNfs2(filename_.c_str(), O_WRONLY|O_SYNC);

  // Blocking loop
  for (int n_tries = 1; (fd < 0) && (n_tries < max_tries); ++n_tries) {
    // Check for stale lockfile
    if ((t_stale >= 0.0) && ((age + n_tries*t_wait) > t_stale)) {
      age = ComputeAge(filename_);
      if (age > t_stale) {
        DeleteStaleLock(filename_, t_stale);
      }
    }

    // Wait before trying again
    nanosleep(&ts_wait, NULL);

    // Try to create the lockfile again
    //fd = open(filename_.c_str(), flags, mode);
    fd = OpenCreatExclNfs2(filename_.c_str(), O_WRONLY|O_SYNC);
  }

  // Write identifying information in lockfile
  if (fd != -1) {
    fchmod(fd, mode|S_IWUSR);
    std::string label = GetProcessLabel();
    const ssize_t bytes_written = write(fd, label.data(), label.size());
    REQUIRE(bytes_written == static_cast<ssize_t>(label.size()),
            "Could not write to lockfile '" + filename_ + "'");
    fchmod(fd, mode);
    const int fsync_ret = fsync(fd);
    REQUIRE(fsync_ret != -1,
            "Could not sync lockfile '" + filename_ + "'; errno=" << errno);
  }

  // Store file descriptor for lockfile (or -1 if failure)
  fd_ = fd;

  return HasLock();
}

void LockfileHandler::Release() {
  REQUIRE(HasLock(),
          "Cannot release lock on '" + filename_ + "' - lock not held\n"
          "This indicates a logic error in the code using this lock.");

  // Close lockfile
  fchmod(fd_, S_IRUSR);
  int close_ret = close(fd_);
  REQUIRE(close_ret == 0, "Could not close lockfile '" + filename_ + "'");

  // Check that it is still your lock
  std::string label = GetProcessLabel();
  int fd = open(filename_.c_str(), O_RDONLY);
  REQUIRE(fd != -1, "Could not re-open held lock on '" + filename_ + "'.\n"
          "Another processes may have considered this lock stale and removed "
          "it.");
  char* buf = new char[label.size()+1];
  const ssize_t bytes_read = read(fd, buf, label.size());
  REQUIRE(bytes_read == static_cast<ssize_t>(label.size()),
          "Did not read expected number of bytes from lockfile '" +
          filename_ + "'");
  buf[label.size()] = '\0';
  std::string lock_str(buf);
  REQUIRE(lock_str == label,
          "Unexpected lock contents in lockfile '" + filename_ + "'.\n"
          "Read '" << lock_str << "', expected '" << label << "'.\n"
          "Another processes may have considered this lock stale and replaced "
          "it.");
  delete[] buf;
  close_ret = close(fd);
  REQUIRE(close_ret == 0,
          "Could not close lockfile '" + filename_ + "' after reading");

  // Delete lockfile
  const int remove_ret = remove(filename_.c_str());
  REQUIRE(remove_ret == 0, "Could not delete lockfile '" + filename_ + "'");
  fd_ = -1;
}

bool LockfileHandler::HasLock() const {
  return fd_ >= 0;
}

double LockfileHandler::ComputeAge(const std::string& filename) {
  const time_t now = time(NULL);
  struct stat stat_buf;
  const int stat_ret = stat(filename.c_str(), &stat_buf);
  if (stat_ret == 0) {
    return difftime(now, stat_buf.st_mtime);
  } else {
    return 0.0;
  }
}

std::string LockfileHandler::GetProcessLabel() const {
  // Should use HOST_NAME_MAX instead of 255, but it is not a compile-time
  //   constant.  Future maintainers should look at sysconf().
  char hostname[255+1];
  gethostname(hostname, 255);
  hostname[255] = '\0';
  const pid_t pid = getpid();
  std::ostringstream str;
  str << "Host: " << hostname << "; PID: " << pid << std::endl;
  return str.str();
}

int LockfileHandler::OpenCreatExclNfs2(const std::string& filename,
                                       const int flags) {
  // Make unique file
  std::string pattern = filename + "_XXXXXX";
  char* pattern_cstr = new char[pattern.size()+1];
  strcpy(pattern_cstr, pattern.c_str());
  const int fd = mkstemp(pattern_cstr);
  if (fd == -1) {
    return -1;
  }
  pattern = pattern_cstr;
  close(fd);

  // Link to desired filename
  const int link_ret = link(pattern.c_str(), filename.c_str());
  struct stat stat_buf;
  const int stat_ret = stat(pattern.c_str(), &stat_buf);
  remove(pattern.c_str());
  delete[] pattern_cstr;
  if ((stat_ret == 0) && (stat_buf.st_nlink == 2)) {
    return open(filename.c_str(), flags);
  } else {
    // This is mostly just to avoid a gcc warning
    if (link_ret == 0) { std::cerr << "link(2) lied" << std::endl; }
    return -1;
  }
}

bool LockfileHandler::DeleteStaleLock(const std::string& filename,
                                      const double t_stale) {
  // If a file is more than a year old, it will not be considered stale
  const double max_stale = 31536000;

  LockfileHandler dellock(filename + ".dellock");
  const bool locked = dellock.Acquire(0.0, t_stale, 1);
  if (!locked) {
    return false;
  }

  bool success = false;
  const double age = ComputeAge(filename);
  if (age > t_stale) {
    if (age > max_stale) {
      // Assume mtime is buggy, so don't remove the file
      std::cerr << "Lockfile too stale for removal: " << filename << std::endl;
    } else {
      // File is stale; try to delete it
      std::cerr << "Removing stale lockfile: " << filename << std::endl;
      const int remove_ret = remove(filename.c_str());
      if (remove_ret == 0) {
        success = true;
      }
    }
  } else {
    // File is not stale (or does not exist), so no action is required
    success = true;
  }

  dellock.Release();
  return success;
}

void LockfileHandler::DoubleToTimespec(const double secs, struct timespec* ts) {
  ASSERT((secs >= 0.0), "'secs' must be non-negative");
  double ipart;
  const double fpart = modf(secs, &ipart);
  const double nanosecs = 1.0e9 * fpart;
  ts->tv_sec = static_cast<time_t>(ipart);
  ts->tv_nsec = static_cast<long>(nanosecs);
}

