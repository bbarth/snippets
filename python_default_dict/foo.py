#!/usr/bin/env python

import collections

def main():
  d=collections.defaultdict(float)

  thing=[('a',1.) , ('a', 2.), ('b', 5.), ('b', 10.), ('c', 100.)]

  for (k,v) in thing:
    d[k]+=v

  for k in d:
    print k, d[k]
  

if __name__ == '__main__':
  main()
