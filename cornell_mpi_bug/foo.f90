program mpimulti_bug
use mpi
use omp_lib
integer :: iprovided, irank, istat(MPI_STATUS_SIZE), ierr
integer :: nt, ithread, ipre, j

call mpi_init_thread(MPI_THREAD_MULTIPLE, iprovided, ierr)
call mpi_comm_rank(MPI_COMM_WORLD, irank, ierr)
if (irank == 0) then
   print '("Thread numbers should match in each row below...")'
end if

if (iprovided >= MPI_THREAD_MULTIPLE) then
! All threads can call MPI
!$OMP parallel private(nt, ithread, ipre, j, istat, ierr)
   nt = omp_get_num_threads()
   ithread = omp_get_thread_num()
   ipre = ithread
   if (irank == 0) then
      call mpi_send(ithread, 1, MPI_INTEGER, 1, ithread, &
         MPI_COMM_WORLD, ierr)
      print '("Rank 0: ithread = ",i3," before send, ",i3,' &
         //'" after send")', ipre, ithread
   elseif (irank == 1) then
      call mpi_recv(j, 1, MPI_INTEGER, 0, ithread, &
         MPI_COMM_WORLD, istat, ierr)
! The tag argument, ithread, should be input-only to mpi_recv -
! Its value should not become 0 following the call to mpi_recv
      print '("Rank 1: ithread = ",i3," before recv, ",i3,' &
         //'" after recv, j = ",i3)', ipre, ithread, j
   endif
!$OMP end parallel
elseif (iprovided == MPI_THREAD_SINGLE) then
   if (irank == 0) print *, "iprovided is MPI_THREAD_SINGLE"
elseif (iprovided == MPI_THREAD_FUNNELED) then
   if (irank == 0) print *, "iprovided is MPI_THREAD_FUNNELED"
elseif (iprovided == MPI_THREAD_SERIALIZE) then
   if (irank == 0) print *, "iprovided is MPI_THREAD_SERIALIZE"
else
   if (irank == 0) print *, "iprovided is not recognized"
end if

call mpi_finalize(ierr)

end program mpimulti_bug
