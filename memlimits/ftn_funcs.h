#include <sys/types.h>

extern "C"
{
  void ftn_dealloc_(char *d);
  void ftn_alloc_(char* d, size_t size);
}
