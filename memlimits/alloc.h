#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <errno.h>
#include <string>

#include "bbpretty.h"
#include "ftn_funcs.h"

class Malloc;
class Realloc;
class Pmemalign;
class Mmap;
class Cppnew;
class Fortran;

class AllocBase
{
public:
  AllocBase():_data(0),_size(0) {};
  virtual ~AllocBase()=0;
  virtual bool allocate(size_t bytes)=0;
  virtual void deallocate()=0;
  virtual void fill();
  static AllocBase* build(std::string& kind); 
protected:
  char*  _data;
  size_t _size;
  char   _ps[256];

  virtual void _print(size_t s);
};

AllocBase::~AllocBase() {}

void AllocBase::fill() // Fill in parallel
{
  printf("Filling\n"); fflush(stdout);
  #pragma omp parallel for
  for (size_t j = 0; j<_size; j++)
    _data[j]=(j*_size)%256;
}

void AllocBase::_print(size_t s)
{
  printf("Allocating %s bytes\n",bbpretty(s,_ps));
}

class Malloc: public AllocBase
{
public:
  Malloc() {}
  virtual ~Malloc() {if (_size>0) free(_data); }
  virtual bool allocate(size_t bytes);
  virtual void deallocate()
  {
    printf("Deallocating\n"); fflush(stdout);
    free(_data);
    _size=0;
  }
};

bool Malloc::allocate(size_t bytes)
{
  _print(bytes);
  _size=bytes;
  _data=(char*)malloc(bytes);
  return _data;
}

class Realloc: public AllocBase
{
public:
  Realloc() {}
  virtual ~Realloc() {if (_size>0) free(_data); }
  virtual bool allocate(size_t bytes);
  virtual void deallocate()
  {
    printf("Skipping deallocate for realloc\n"); fflush(stdout);
  }
};

bool Realloc::allocate(size_t bytes)
{
  _print(bytes);
  _size=bytes;
  _data=(char*)realloc(_data,bytes);
  return _data;
}

class Pmemalign: public AllocBase
{
public:
  Pmemalign() {}
  virtual ~Pmemalign() {if (_size>0) free(_data); }
  virtual bool allocate(size_t bytes);
  virtual void deallocate()
  {
    printf("Deallocating\n"); fflush(stdout);
    free(_data);
    _size=0;
  }
};

bool Pmemalign::allocate(size_t bytes)
{
  _print(bytes);
  _size=bytes;
  posix_memalign((void **)(&_data),4096,bytes);
  return _data;
}

class Mmap: public AllocBase
{
public:
  Mmap() {}
  virtual ~Mmap() { }
  virtual bool allocate(size_t bytes);
  virtual void deallocate()
  {
    printf("Deallocating\n"); fflush(stdout);
    munmap(_data,_size);
    _size=0;
  }
};

bool Mmap::allocate(size_t bytes)
{
  //  _size=(bytes/4096)*4096;
  _size=bytes;
  _print(_size);
  _data=(char*)mmap(NULL,_size,PROT_READ|PROT_WRITE,
                    MAP_ANONYMOUS|MAP_NORESERVE|MAP_PRIVATE, 0, 0);
  if (_data==MAP_FAILED)
    {
      perror("Failed to map");
      _data=NULL;
      _size=0;
    }  
  return _data;
}

class Cppnew: public AllocBase
{
public:
  Cppnew() {}
  virtual ~Cppnew() {if (_size>0) delete [] _data; }
  virtual bool allocate(size_t bytes);
  virtual void deallocate()
  {
    printf("Deallocating\n"); fflush(stdout);
    delete [] _data;
  }
};

bool Cppnew::allocate(size_t bytes)
{
  //  _size=(bytes/4096)*4096;
  _size=bytes;
  _print(_size);
  try
    {
      _data=new char[_size];
    }
  catch(std::bad_alloc& e)
    {
      _data=NULL;
      _size=0;
    }
  return _data;
}

class Fortran: public AllocBase
{
public:
  Fortran() {}
  virtual ~Fortran() {if (_size>0) ftn_dealloc_(_data); }
  virtual bool allocate(size_t bytes);
  virtual void deallocate()
  {
    printf("Deallocating\n"); fflush(stdout);
    ftn_dealloc_(_data);
  }
};

bool Fortran::allocate(size_t bytes)
{
  //  _size=(bytes/4096)*4096;
  _size=bytes;
  _print(_size);
  ftn_alloc_(_data,_size);
  return _data;
}

AllocBase* AllocBase::build(std::string& kind)
{
  if (kind.compare("malloc")==0)
    return new Malloc;
  else if (kind.compare("realloc")==0)
    return new Realloc;
  else if (kind.compare("posix_memalign")==0)
    return new Pmemalign;
  else if (kind.compare("mmap")==0)
    return new Mmap;
  else if (kind.compare("new")==0)
    return new Cppnew;
  else if (kind.compare("fortran")==0)
    return new Fortran;
  return NULL;
}
  
