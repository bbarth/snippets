#include <stdio.h>
#include "bbpretty.h"

char *bbpretty(size_t size, char* buf)
{
  if(!buf) return(0);
  else buf[0]=0;

  size_t k=1024L;
  size_t m=k*k;
  size_t g=m*k;

  long double ksize=((long double) size)/((long double) k);
  long double msize=((long double) size)/((long double) m);
  long double gsize=((long double) size)/((long double) g);

  if(size <=k)
    sprintf(buf,"%6.6fB",(double) size);
  else if(size <=m)
    sprintf(buf,"%6.6fkB",(double) ksize);
  else if(size <=g)
    sprintf(buf,"%6.6fMB",(double) msize);
  else
    sprintf(buf,"%6.6fGB",(double) gsize);

  return(buf);
}
