#include <string.h>

#if defined __cplusplus
extern "C" {
#endif

char *bbpretty(size_t size, char* buf);

#if defined __cplusplus
}
#endif

