#include "mpi.h"
#include "stdio.h"
#include "signal.h"

int main(int argc, char* argv[])
{
  int ierr, myProc, nProc;

  ierr = MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &myProc);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);

  printf("Hello from: %d of %d\n",myProc,nProc);

  if (myProc == 0)
    raise(SIGFPE);

  

  MPI_Finalize();

  return 0;
}
