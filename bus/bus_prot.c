// Version of bus error causing program that uses mprotect()
#include <stdlib.h>
#include <stdio.h>

#include <errno.h>
#include <string.h>

#include <sys/types.h>

#define N 1000

#include <sys/mman.h>
int main()
{
  char *buf = NULL;
  int res=0;

  posix_memalign(&buf,4096,N);

  printf("%p\n",&buf[0]);
  
  if (buf)
    {
      if (!(res=mprotect(&buf[0], N, PROT_NONE)))
	{
	  printf("%d\n",res);
	  perror("mprotect:" );
	  buf[0] = 0;
	}

    }
  return 0;
}
