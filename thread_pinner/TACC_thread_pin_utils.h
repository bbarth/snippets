#ifndef _TACC_THREAD_PIN_UTILS_H
#define _TACC_THREAD_PIN_UTILS_H
#include <unistd.h>

#include "mpi.h"
#include "omp.h"

int TACC_get_node_count(MPI_Comm comm);
void TACC_print_affinity(char *header);
static int qsort_compare (const void * a, const void * b);

#endif
