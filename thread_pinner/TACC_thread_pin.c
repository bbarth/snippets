/* Given an MPI communicator, this routine pins each MPI task and its child
   threads to subsequent cores.

   This function considers a homogeneous system and places a balanced number
   of workers on each socket.

*/

/* This must go here, and it must be first!
   DO NOT MOVE */
#ifndef _GNU_SOURCE 
#define _GNU_SOURCE
#endif
#include <sched.h>
/* END DO NOT MOVE */

#include "TACC_thread_pin.h"
#include "TACC_thread_pin_utils.h"

#include <stdio.h>

void TACC_thread_pin(MPI_Comm comm)
{
  int nodes=TACC_get_node_count(comm);
  int size;
  MPI_Comm_size(comm,&size);
  int mpi_tasks_per_node=size/nodes;

  TACC_thread_pin_2(comm,mpi_tasks_per_node);
}

void TACC_thread_pin_2(MPI_Comm comm, int mpi_tasks_per_node)
{
  long cores_per_node=sysconf(_SC_NPROCESSORS_ONLN);

  int my_rank, num_ranks;
  MPI_Comm_rank(comm, &my_rank);
  MPI_Comm_size(comm, &num_ranks);

#pragma omp parallel
  {
    int omp_id=omp_get_thread_num();
    int scale=cores_per_node/mpi_tasks_per_node;
    int local_rank=my_rank % mpi_tasks_per_node;
    int core=local_rank*scale+omp_id;
    cpu_set_t mask;

    CPU_ZERO(&mask);
    CPU_SET(core,&mask);
    sched_setaffinity(0,sizeof(cpu_set_t),&mask);
  }
}

// Fortran wrappers
void tacc_thread_pin_(MPI_Fint *fcomm, int *mpi_tasks_per_node)
{
  MPI_Comm c=MPI_Comm_f2c(*fcomm);
  TACC_thread_pin(c);
}

