#ifndef _TACC_THREAD_PIN_H
#define _TACC_THREAD_PIN_H

#include <unistd.h>

#include "mpi.h"
#include "omp.h"

int TACC_get_node_count(MPI_Comm comm);
void TACC_thread_pin(MPI_Comm);
void TACC_thread_pin_2(MPI_Comm comm, int mpi_tasks_per_node);
#endif
