program fdriver

  use mpi
  implicit none 
  
  integer :: ierr, myProc, nProc, mpi_tasks_per_node
  
  call MPI_init(ierr)

  call MPI_Comm_rank(MPI_COMM_WORLD, myProc, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, nProc,  ierr)

  mpi_tasks_per_node=4

  if (myProc .eq. 0) then
    print*, 'Initial pinning'
  end if 
  call MPI_Barrier(MPI_COMM_WORLD,ierr)
  call TACC_print_affinity()

  call TACC_thread_pin(MPI_COMM_WORLD,mpi_tasks_per_node)

  call MPI_Barrier(MPI_COMM_WORLD,ierr)
  
  if (myProc .eq. 0) then
    print*, 'After pin call'
  end if 
  call MPI_Barrier(MPI_COMM_WORLD,ierr)
  call TACC_print_affinity()

  call MPI_Finalize(ierr);

end program fdriver
 

