/* This must go here, and it must be first!
   DO NOT MOVE */
#ifndef _GNU_SOURCE 
#define _GNU_SOURCE
#endif
#include <sched.h>
/* END DO NOT MOVE */

#include "TACC_thread_pin_utils.h"

/* Get the number of nodes in a communicator by uniquing the hostnames */

int TACC_get_node_count(MPI_Comm comm)
{
  char mpiHostNm[MPI_MAX_PROCESSOR_NAME];
  int hostNmLen;
  int ierr, iproc, icore;
  char *  hostNm;
  char *  hostNmBuf;
  char ** hostNmA;
  char *  p;
  int     nlenL, nlen, nCores;
  int     nNodes, nCoresMax, myProc, nProc;
  
  MPI_Comm_size(comm,&nProc);
  MPI_Get_processor_name(&mpiHostNm[0],&hostNmLen);
  nlenL = hostNmLen;
  ierr = MPI_Allreduce(&nlenL, &nlen, 1, MPI_INT, MPI_MAX, comm);

  hostNm = (char *) malloc(nlen+1);
  strcpy(hostNm, mpiHostNm);

  hostNmBuf = (char*)  malloc((nlen+1)*nProc);
  hostNmA   = (char**) malloc(nProc*sizeof(char*));

  ierr = MPI_Allgather(&hostNm[0],    nlen+1, MPI_CHAR,
                       &hostNmBuf[0], nlen+1, MPI_CHAR, comm);

  p = &hostNmBuf[0];
  
  for (iproc = 0; iproc < nProc; ++iproc)
    {
      hostNmA[iproc] = p;
      p += nlen + 1;
    }
  
  qsort (hostNmA, nProc, sizeof (const char *), qsort_compare);
  nNodes    = 1;
  p         = hostNmA[0];
  nCores    = 1;
  nCoresMax = 0;

  for (iproc = 1; iproc < nProc; ++iproc)
    {
      if (strcmp(hostNmA[iproc], p) == 0)
        nCores++;
      else
        {
          if (nCores > nCoresMax)
            nCoresMax = nCores;
          nNodes++;
          p = hostNmA[iproc];
          nCores = 0;
        }
    }
  if (nCoresMax == 0)
    nCoresMax = nCores;
  free(hostNm);
  free(hostNmBuf);
  free(hostNmA);
  //  *numCoresPer = nCoresMax;
  //  *numNodes    = nNodes;
  return nNodes;
}

static int qsort_compare (const void * a, const void * b)
{
  /* The pointers point to offsets into "array", so we need to
     dereference them to get at the strings. */

  return strcmp (*(const char **) a, *(const char **) b);
}

void TACC_print_affinity(char *header)
{
#pragma omp parallel
  {
    cpu_set_t mask;
    char *core_string;
    int nt=omp_get_num_threads();
    long cores_per_node=sysconf(_SC_NPROCESSORS_ONLN);
    int i;
    core_string=(char*)calloc((cores_per_node+1),sizeof(char));
    if(core_string==NULL)
      exit(1);
    
    CPU_ZERO(&mask);
    sched_getaffinity(0,sizeof(cpu_set_t),&mask);
    for(i=0; i < cores_per_node; ++i)
      sprintf(&core_string[i],"%1d",CPU_ISSET(i,&mask));
#pragma omp barrier
#pragma omp critical
    {
      if (header ==NULL)
	printf("%s \n",core_string);
      else
	printf("%s %s \n",header,core_string);
    }
    free(core_string);
  }
  
}

void tacc_print_affinity_()
{
  TACC_print_affinity(NULL);
}
