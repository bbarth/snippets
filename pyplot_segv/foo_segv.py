#!/bin/sh
# -*- python -*-
# This file is python bilingual: it starts a comment in Python,
# and is a no-op in shell

""":"
# Find a suitable python interpreter 
for cmd in python3 python python2; do
command -v > /dev/null $cmd && exec $cmd $0 "$@"
done

echo "Error: Could not find a valid python interpreter --> exiting!"
exit 2
":"""

from __future__ import print_function
import os, sys, re

#import faulthandler; faulthandler.enable()

import matplotlib

#matplotlib.use('Agg') # I thought this was the default, but it seems to be
#GTK3Agg or GTK3Cairo on RHEL7/CENTOS7 systems at TACC which aren't currently
#installed right. Once I can push from FTA to bitbucket, I'll try this on S2 and
#see if it fails there, too. Evidence from LS5 indicates that Cairo is required
#to get the GTK3Agg backend, which isn't installed on LS5, and won't pip install
#b/c the underlying requirements aren't there. 

# import pdb; pdb.set_trace()
try:
  from matplotlib import pyplot as plt
except:
  pass

def main():
  print ("Hello World!")

if ( __name__ == '__main__'): main()

