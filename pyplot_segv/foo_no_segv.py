#!/bin/sh
# -*- python -*-
# This file is python bilingual: it starts a comment in Python,
# and is a no-op in shell

""":"
# Find a suitable python interpreter 
for cmd in python3 python python2; do
command -v > /dev/null $cmd && exec $cmd $0 "$@"
done

echo "Error: Could not find a valid python interpreter --> exiting!"
exit 2
":"""

from __future__ import print_function
import os, sys, re

import matplotlib
matplotlib.use('pdf')
from matplotlib import pyplot as plt

def main():
  print ("Hello World!")

if ( __name__ == '__main__'): main()

