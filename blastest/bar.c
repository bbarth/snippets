#include "mkl_cblas.h"

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#define __USE_MISC
#include <sys/mman.h>


int main(int argc, char** argv)
{
  //  printf("argc: %d\n",argc);
  if (argc < 3)
    {
      printf("usage: %s n maxiters\n",argv[0]);
      exit(0);
    }
  int n=atoi(argv[1]);
  int maxiters=atoi(argv[2]);

  double *a, *b, *c;

  posix_memalign(&a,64,sizeof(double)*n*n);
  posix_memalign(&b,64,sizeof(double)*n*n);
  posix_memalign(&c,64,sizeof(double)*n*n);

  size_t sz=n*n;

  //  printf("%zd\n",sz);

  //  double *pool=(double*)mmap(0,3*sizeof(double)*sz,PROT_READ|PROT_WRITE,
  //                             MAP_ANONYMOUS|MAP_PRIVATE|MAP_HUGETLB,-1,0);

//  a=pool;
//  b=pool+sz;
//  c=b+sz;

  //  printf("%p %p %p\n",a,b,c);fflush(stdout);

  for (int i=0; i<n; ++i)
    {
      for (int j=0; j<n; ++j)
        {
          a[j+n*i]=(double)(i+1)/(double)(i*j+1);
          b[j+n*i]=(double)(j+1)/(double)(i*j+1);
          c[j+n*i]=0.;
        }
    }

  //  printf("%p %p %p\n",a,b,c);fflush(stdout);

  struct timeval ts, te;
  
  gettimeofday(&ts,NULL);
  
  long long flops=0.;
  for (int its=0; its<maxiters; ++its)
    {
      cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,
                  n,n,n,1.,a,n,b,n,0.,c,n);
      flops+=((long long)(n)*(long long)(n)*(long long)(n)*2);
    }

  gettimeofday(&te,NULL);

  printf("%p %p %p\n",a,b,c);fflush(stdout);

  double ds=(double)(ts.tv_sec) + 1.e-6*(double)(ts.tv_usec);
  double de=(double)(te.tv_sec) + 1.e-6*(double)(te.tv_usec);

  double time=(de-ds);


  printf("size: %6d, iters: %7d, Flops: %8.7g, Gflop/s: %8.7g, time: %8.5g\n",n,maxiters,((double)flops)/maxiters,(double)((double)(flops))/time/1e9,time/maxiters);

  return(0);
}
