#include "mkl_cblas.h"

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

int main(int argc, char** argv)
{
  printf("argc: %d\n",argc);
  if (argc < 3)
    {
      printf("usage: %s n maxiters\n",argv[0]);
      exit(0);
    }
  int n=atoi(argv[1]);
  int maxiters=atoi(argv[2]);

  float *a, *b, *c;

  posix_memalign(&a,64,sizeof(float)*n*n);
  posix_memalign(&b,64,sizeof(float)*n*n);
  posix_memalign(&c,64,sizeof(float)*n*n);

  for (int i=0; i<n; ++i)
    {
      for (int j=0; j<n; ++j)
        {
          a[j+n*i]=(float)(i+1)/(float)(i*j+1);
          b[j+n*i]=(float)(j+1)/(float)(i*j+1);
          c[j+n*i]=0.;
        }
    }

  struct timeval ts, te;
  
  gettimeofday(&ts,NULL);
  
  long long flops=0.;
  for (int its=0; its<maxiters; ++its)
    {
      cblas_sgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,
                  n,n,n,1.,a,n,b,n,0.,c,n);
      flops+=((long long)(n)*(long long)(n)*(long long)(n)*2);
    }

  gettimeofday(&te,NULL);

  double ds=(double)(ts.tv_sec) + 1.e-6*(double)(ts.tv_usec);
  double de=(double)(te.tv_sec) + 1.e-6*(double)(te.tv_usec);

  double time=(de-ds);


  printf("size: %6d, iters: %7d, Flops: %8.7g, Mflop/s: %8.7g, time: %8.5g\n",n,maxiters,((double)flops)/maxiters,(double)((double)(flops))/time/1e6,time/maxiters);

  return(0);
}
