#!/bin/sh

iters=200
numelem=100

export LD_LIBRARY_PATH=/from_host
export OMP_NUM_THREADS=60
export MKL_NUM_THREADS=1
export KMP_AFFINITY=compact,1

for i in 10 100 200 300 400 500 600 700 800 900 1000 1024; do
  memory=$(($i*$i*8*2*100))
  /from_host/bbarth/foo $i $memory $iters
done