#include "mkl_cblas.h"

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>


void dumb_dgemv(int n, double* a, double* x, double *y)
{
  for (int i=0; i<n; ++i)
    {
      for (int j=0; j<n; ++j)
        {
          y[i]+=a[j+n*i]*x[j];
        }
    }
 }

int main(int argc, char** argv)
{
  int n=atoi(argv[1]);
  size_t memsize=atoll(argv[2]);
  int maxiters=atoi(argv[3]);

  int numel=memsize/(2*n*n)/8;

  if(numel < 1)
    exit(0);

  printf("Elements: %d\n",numel);

  double **a, **b, **x, **y, **z;

  a=malloc(sizeof(double*)*numel);
  b=malloc(sizeof(double*)*numel);
  x=malloc(sizeof(double*)*numel);
  y=malloc(sizeof(double*)*numel);
  z=malloc(sizeof(double*)*numel);

  for (int e=0; e<numel; e++)
    {
      posix_memalign(&a[e],64,sizeof(double)*n*n);
      posix_memalign(&b[e],64,sizeof(double)*n*n);
      posix_memalign(&x[e],64,sizeof(double)*n);
      posix_memalign(&y[e],64,sizeof(double)*n);
      posix_memalign(&z[e],64,sizeof(double)*n);
    }

  for (int e=0; e<numel; e++)
    {
      for (int i=0; i<n; ++i)
        {
          for (int j=0; j<n; ++j)
            {
              a[e][j+n*i]=(double)(e+1)/(double)(i*j+1);
            }
          x[e][i]=(double) i;
          y[e][i]=0.;
          z[e][i]=0.;
        }
    }

  
  struct timeval ts, te;

  gettimeofday(&ts,NULL);

  double flops=0.;
  for (int its=0; its<maxiters; ++its)
    {
#pragma omp parallel for reduction (+:flops)
      for (int e=0; e<numel; e++)
        {
          cblas_dgemv(CblasRowMajor,CblasNoTrans,
                      n,n,1.,a[e],n,x[e],1,1.,y[e],1);
          flops+=(double)(2*n*n);
        }
    }

  gettimeofday(&te,NULL);

  double ds=(double)(ts.tv_sec) + 1.e-6*(double)(ts.tv_usec);
  double de=(double)(te.tv_sec) + 1.e-6*(double)(te.tv_usec);

  double time=(de-ds);


  printf("size: %6d, iters: %7d, Flops: %8.7g, Mflop/s: %8.7g, time: %8.5g\n",n,maxiters,flops/maxiters,flops/time/1e6,time/maxiters);

  
  gettimeofday(&ts,NULL);

  flops=0.;
  for (int its=0; its<maxiters; ++its)
    {
#pragma omp parallel for reduction(+:flops)
      for (int e=0; e<numel; e++)
        {
          dumb_dgemv(n,a[e],x[e],z[e]);
          flops+=(double)(2*n*n);
        }
    }

  gettimeofday(&te,NULL);

  ds=(double)(ts.tv_sec) + 1.e-6*(double)(ts.tv_usec);
  de=(double)(te.tv_sec) + 1.e-6*(double)(te.tv_usec);

  time=(de-ds);

  printf("size: %6d, iters: %7d, Flops: %8.7g, Mflop/s: %8.7g, time: %8.5g\n",n,maxiters,flops/maxiters,flops/time/1e6,time/maxiters);

  double sdiff=0.;
  for (int e=0; e<numel; e++)
    {
      for (int i=0; i<n; ++i)
        {
          sdiff+=(y[e][i]-z[e][i])*(y[e][i]-z[e][i]);
        }
    }

  printf("diff: %g\n",sdiff);
}
