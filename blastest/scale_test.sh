#!/bin/sh

maxiters=5

#export LD_LIBRARY_PATH=/from_host
#export MKL_NUM_THREADS=1
export MIC_KMP_AFFINITY=compact

#for t in 1 2  4 8 16 30 32 40 50 64 60 70 80 90 100 110 120; do
for t in 1 2  4 5 6 8 16 30; do
  for i in $(seq 100 100 4000) ; do
    if [ $i -lt 1000 ]; then
      iters=$maxiters
    else
      iters=1
    fi
    export MIC_OMP_NUM_THREADS=$t  
    echo -n "$t: "
    `pwd`/bar $i $iters
  done
done