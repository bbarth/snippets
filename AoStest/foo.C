#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>

#include <iostream>

#include <vector>

#define GET_DOUBLE(ts) (double)((ts).tv_sec) + 1.e-6*(double)((ts).tv_usec)

using namespace std;

struct Point3
{
  double x, y, z;
};

struct PointArr3
{
  double x[3];
};

struct Points
{
  vector<double> x, y, z;
};
  


int main(int argc, char **argv)
{

  if (argc < 3)
    exit(1);

  int n=atoi(argv[1]);
  int maxiters=atoi(argv[2]);

  cout << "Maximum iterations: " << maxiters << endl;

  vector<Point3> a,b;
  vector<PointArr3> c,d;
  vector<double> e,f;
  Points g,h;

  cout << "Allocating Space: " << n << endl;

  a.resize(n);
  b.resize(n);
  c.resize(n);
  d.resize(n);
  e.resize(3*n);
  f.resize(3*n);
  g.x.resize(n);
  g.y.resize(n);
  g.z.resize(n);
  h.x.resize(n);
  h.y.resize(n);
  h.z.resize(n);

  cout << "Initializing Arrays" << endl;
  
  for (int i=0; i < n; ++i)
    {
      double t=1./(1.+(double) i);
      double t2=1./(1.+(double) (n-i));
      a[i].x=t;
      a[i].y=t;
      a[i].z=t;

      b[i].x=t2;
      b[i].y=t2;
      b[i].z=t2;

      c[i].x[0]=t;
      c[i].x[1]=t;
      c[i].x[2]=t;
      
      d[i].x[0]=t2;
      d[i].x[1]=t2;
      d[i].x[2]=t2;

      e[3*i+0]=t;
      e[3*i+1]=t;
      e[3*i+2]=t;

      f[3*i+0]=t2;
      f[3*i+1]=t2;
      f[3*i+2]=t2;

      g.x[i]=t;
      g.y[i]=t;
      g.z[i]=t;

      h.x[i]=t2;
      h.y[i]=t2;
      h.z[i]=t2;
    }

  struct timeval ts, te;

  gettimeofday(&ts,NULL);

  cout << "Computing with Point3" << endl;

  double diff=0.;
#pragma novector
  for(int it=0; it<maxiters; ++it)
    {  
      for (int i=0; i < n; ++i)
        {
          diff += ( (a[i].x-b[i].x)*(a[i].x-b[i].x) +
                    (a[i].y-b[i].y)*(a[i].y-b[i].y) +
                    (a[i].z-b[i].z)*(a[i].z-b[i].z) );
        }
    }

  gettimeofday(&te,NULL);

  cout << "Time: " << GET_DOUBLE(te) - GET_DOUBLE(ts)
       << " Diff: " << diff << endl;

  gettimeofday(&ts,NULL);

  cout << "Computing with PointArr3" << endl;

  diff=0.;
#pragma novector
  for(int it=0; it<maxiters; ++it)
    {  
      for (int i=0; i < n; ++i)
        {
          diff += ( (c[i].x[0]-d[i].x[0])*(c[i].x[0]-d[i].x[0]) +
                    (c[i].x[1]-d[i].x[1])*(c[i].x[1]-d[i].x[1]) +
                    (c[i].x[2]-d[i].x[2])*(c[i].x[2]-d[i].x[2]) );
        }
    }

  gettimeofday(&te,NULL);

  cout << "Time: " << GET_DOUBLE(te) - GET_DOUBLE(ts)
       << " Diff: " << diff << endl;

  gettimeofday(&ts,NULL);

  cout << "Computing with Stride One Compact mode" << endl;
  diff=0.;
#pragma novector
  for(int it=0; it<maxiters; ++it)
    {  
      for (int i=0; i < n; ++i)
        {
          diff += ( (e[3*i+0]-f[3*i+0])*(e[3*i+0]-f[3*i+0]) +
                    (e[3*i+1]-f[3*i+1])*(e[3*i+1]-f[3*i+1]) +
                    (e[3*i+2]-f[3*i+2])*(e[3*i+2]-f[3*i+2]) );
        }
    }

  gettimeofday(&te,NULL);

  cout << "Time: " << GET_DOUBLE(te) - GET_DOUBLE(ts)
       << " Diff: " << diff << endl;
  
  gettimeofday(&ts,NULL);

  cout << "Computing with Struct of Arrays" << endl;
  diff=0.;
#pragma novector
  for(int it=0; it<maxiters; ++it)
    {  
      for (int i=0; i < n; ++i)
        {
          diff += ( (g.x[i]-h.x[i])*(g.x[i]-h.x[i]) +
                    (g.y[i]-h.y[i])*(g.y[i]-h.y[i]) +
                    (g.z[i]-h.z[i])*(g.z[i]-h.z[i]) );
        }
    }

  gettimeofday(&te,NULL);

  cout << "Time: " << GET_DOUBLE(te) - GET_DOUBLE(ts)
       << " Diff: " << diff << endl;

  return(0);
}
