#include <iostream>

#include <unistd.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char* argv[])
{
  int a=1;
  while (1)
    {
      int usl=atoi(argv[1]);
      usleep(usl);

      int maxIts=atoi(argv[2]);
      
      for (int i=0; i<maxIts; ++i)
        {
          a+=i;
        }
    }
  
  return (a%128);
}
