#include <iostream>
#include <iomanip>
#include <cfloat>
#include <vector>

#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

extern "C"
{
  float get_TSC_frequency();
}

enum TimerType {GTOD=0, RDTSCP}; // move to mytimer header after making it exist


#define MPICH_SKIP_CXX
#include "mpi.h"
#define MCW MPI_COMM_WORLD

template <typename T>
T bdot(std::vector<T> v1, std::vector<T> v2)
{
  T sum=0.;
  for (int i=0; i< v1.size(); i++)
    sum+=v1[i]*v2[i];
  return sum;
}


using namespace std;

extern "C"
{
  double mytimer(int which);
}

int main(int argc, char* argv[])
{
  enum Mode { AR=0, Work=1};

  int ierr, myProc, nProc;

  ierr = MPI_Init(&argc, &argv);
  
  MPI_Comm_rank(MPI_COMM_WORLD, &myProc);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);

  if (argc != 3)
    MPI_Abort(MCW,1);
  
  int maxIts=atoi(argv[1]);
  int N=atoi(argv[2]);

  double t1=mytimer(1),t2=DBL_MAX;
  double tot=(double )myProc;
  double tot2=0.;
  double sum=0.;
  int its=0;

  double mydiff=0.;

  //  vector<double> diffs(maxIts,0.);
 
  vector < vector < double > > t;

  t.resize(maxIts+1);

  for(its=0; its<maxIts; ++its)
    t[its].resize(3);

  vector<double> v1(N/nProc,(double)myProc);
  vector<double> v2(N/nProc,(double)myProc+1.0);
      
  for (its=0; its<maxIts; ++its)
    {
      t[its][0]=mytimer(GTOD);
      MPI_Allreduce(&tot,&tot2,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      t[its][1]=mytimer(GTOD);
      tot=bdot<double>(v1,v2);
      t[its][2]=mytimer(GTOD);
    }

  MPI_Barrier(MCW);

  if (myProc==0)
    cout << "Looking for bad stuff" << endl;

  // Find jitter incidents

  vector<double> avg(2,0.);
  vector< vector<double> > diffs(maxIts, vector<double>(2,0.));

  if (myProc==0)
    {
      cout << "Quick Test: " << t[0][1] << " " << t[0][0] << " " << t[0][1]-t[0][0] << endl;
    }
  
  for (its=0; its<maxIts; ++its)
    {
      diffs[its][AR]=t[its][1]-t[its][0];
      diffs[its][Work]=t[its][2]-t[its][1];
      avg[AR]+=diffs[its][AR];
      avg[Work]+=diffs[its][Work];
    }
  
  avg[0]/=(double)maxIts;
  avg[1]/=(double)maxIts;


  MPI_Barrier(MCW);
  cout << setprecision(20) << myProc << " AR: " << avg[AR] << " Work: " << avg[Work]
       << " " << tot2 << endl;
  MPI_Barrier(MCW);

  // Gather diffs to 0

#define ADIFFS(it,ab) sbuf[(it)*2 + (ab)]
#define ADIFFR(proc,it,ab) rbuf[(proc)*maxIts*2 + (it)*2 + (ab)]
  vector<double> sbuf(maxIts*2,0.);
  for (its=0; its<maxIts; ++its)
    {
      ADIFFS(its,AR)=diffs[its][AR];
      ADIFFS(its,Work)=diffs[its][Work];
    }
  // Strip to single array for send buffer

  unsigned long long bits=0x7ff0dead7ff0beefULL;
  double dead_beef  = reinterpret_cast<double&>(bits);

  
  vector<double> rbuf(maxIts*2*sizeof(double)*nProc,dead_beef);

  ierr=MPI_Gather(&sbuf[0],maxIts*2,MPI_DOUBLE,
                  &rbuf[0],maxIts*2,MPI_DOUBLE,0,MCW);

  if(myProc == 0)
    {
      for (int p=0; p<nProc; ++p)
        {
          for (int it=0; it<maxIts; ++it)
            {
              
              if(ADIFFR(p,it,AR) > 10. * ADIFFR(p,it,Work))
                cout << p << ": Iteration: " << it << " AR: " << ADIFFR(p,it,AR) << " Work: " << ADIFFR(p,it,Work)<< endl;
            }
        }
    }
  MPI_Barrier(MCW);
  //  cout << "Done: " << myProc << endl;


  MPI_Finalize();

  return 0;
}


