clear all
close all

np=64;
a=csvread('foo_100k_64p.csv');

disp('Data loaded');

ind=cell(1,np);
for i=1:np
  ind{i}=find(a(:,1)==(i-1));
end

c='krgbmc';

figure(1);
hold on
for i=1:np
  semilogy(a(ind{i},3),c(mod(i,length(c))))
end