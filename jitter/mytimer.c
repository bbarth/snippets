#include <sys/time.h>
#include <time.h>
#include <stdio.h>

#include "tacc_timers.h"

enum TimerType {GTOD=0, RDTSCP};

double mytimer_gtod()
{
  struct timeval t;
  gettimeofday(&t,NULL);
  return ((double)t.tv_sec+1.e-6*(double)t.tv_usec);
}

double mytimer_rdtscp()
{
  static unsigned long int orig=0;
  if (!orig)
    orig=rdtscp();
  unsigned long res=rdtscp()-orig;
  double freq=get_TSC_frequency();
  double retval=((double) res)/freq;
  return retval;
}


double mytimer(int which)
{
  if (which==0)
    return mytimer_gtod();
  else
    return mytimer_rdtscp();
}

double mytimer_(int which)
{
  return mytimer(which);
}

double mytimer__(int which)
{
  return mytimer(which);
}

double MYTIMER_(int which)
{
  return mytimer(which);
}

double MYTIMER(int which)
{
  return mytimer(which);
}
