#!/bin/bash

gcc -Wall -Werror -g -shared -o libinsert.so -fPIC insert.c -fPIC  -ldl
gcc -o hello hello.c

echo "Try executing: export LD_PRELOAD=$PWD/libinsert.so ; ./hello; unset LD_PRELOAD"
