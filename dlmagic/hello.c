#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

int main()
{
  char *buf="Hello World\n";
  write(1, &buf[0], strlen(buf)); 
  return(0);
}
