#define _GNU_SOURCE
#include <dlfcn.h>

#include <stdio.h>
#include <unistd.h>

//#include "../jitter/mytimer.c"

// Type that mirrors the signature of write(2)
typedef ssize_t (*orig_write_f_type)(int fd, const void *buf, size_t count);
 
ssize_t write(int fd, const void *buf, size_t count)
{
  // Get a pointer to the original write()
  orig_write_f_type orig_write;
  orig_write = (orig_write_f_type)dlsym(RTLD_NEXT,"write");

  if (orig_write) // If we found it, print out where it came from
    {
      Dl_info dl;
      dladdr(orig_write,&dl);
      printf("got: %s\n", dl.dli_fname);
    }

  // Do something

  printf("In write: %d %zu\n",fd,count);

  // Call the original write
 
  ssize_t res=orig_write(fd,buf,count);

  // Print what it returned

  printf("Wrote %zu bytes\n",res);

  // return that
  return res;
}
