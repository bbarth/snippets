#!/usr/bin/env python

import multiprocessing

def f(d,x,y):
  d[x]=y
def do_work(a):
  f(*a)
    

def main():
  pool = multiprocessing.Pool(processes=2)
  m    = multiprocessing.Manager()
  d    = m.dict()

  pool.map(do_work,[(d,1,2),(d,3,4),(d,5,6),(d,7,8)])

  print d

if __name__ == "__main__":
  main()
