#!/usr/bin/env python

import functools

def f(a,b,c):
  print a
  print b
  print c

def main():
  pf=functools.partial(f,b=1,c=2)

  pf(123)

if __name__ == "__main__":
  main()
