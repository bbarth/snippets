#include <stdio.h>

unsigned long rdpmc_actual_cycles()
{
   unsigned a, d, c;

   c = (1<<30)+1;
   __asm__ volatile("rdpmc" : "=a" (a), "=d" (d) : "c" (c));

   return ((unsigned long)a) | (((unsigned long)d) << 32);;
}


unsigned long tacc_rdtscp(int *chip, int *core)
{
   unsigned a, d, c;

   __asm__ volatile("rdtscp" : "=a" (a), "=d" (d), "=c" (c));
	*chip = (c & 0xFFF000)>>12;
	*core = c & 0xFFF;

   return ((unsigned long)a) | (((unsigned long)d) << 32);;
}

unsigned long rdtscp()
{
   unsigned a, d, c;

   __asm__ volatile("rdtscp" : "=a" (a), "=d" (d), "=c" (c));

   return ((unsigned long)a) | (((unsigned long)d) << 32);;
}


#define N 1024
#define SZ 1200
#define IT 11
void foo()
{
  int chip=0,core=0;
  static double a[SZ], b[SZ];
  static unsigned long d[IT];
  
  double alpha=.95;
  unsigned long t1=0, t2=0;

  for (unsigned long i=0; i<IT; ++i)
    d[i]=0;

  for (int o=0; o<IT; ++o)
    {
      for (int i=0; i<N; ++i)
        {
          a[i]=0.;
          b[i]=(double) i;
        }
      if (o==0)
        {
          unsigned long a_addr=(unsigned long)(&a[0]);
          unsigned long b_addr=(unsigned long)(&b[0]);
          printf("%x %x\n",(a_addr%4096)>>6,(b_addr%4096)>>6);
        }
      
      t1=0;
      t2=0;
      t1=rdpmc_actual_cycles();//tacc_rdtscp(&chip,&core);
      for (int i=0; i<N; ++i)
        a[i]=a[i]+alpha*b[i];
      t2=rdpmc_actual_cycles();
      d[o]=t2-t1;
    }

  double avg=0.;
  for(int i=0; i<IT; ++i)
    avg+=d[i];

  avg/=((double) IT);
  
  printf("time diff: %g %d %d\n",avg, chip, core);
}
