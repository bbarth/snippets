#include <iostream>
#include <vector>
#include <boost/multi_array.hpp>

#include <sys/time.h>
#include <time.h>
#include <stdlib.h>

using namespace std;
using namespace boost;

double fRand(double fMin, double fMax)
{
  double f = (double)rand() / (RAND_MAX + 1.0f);
  return fMin + f * (fMax - fMin);
}

double mytimer()
{
  struct timeval t;
  gettimeofday(&t,NULL);
  return ((double)t.tv_sec+1.e-6*(double)t.tv_usec);
}

// #define Nelm 10000
// #define N 100

unsigned int N;
unsigned int Nelm;

double c99_triple(unsigned int entry)
{
  double a[Nelm][N][N];
  double b[N], c[N];

  for (unsigned int i=0; i < N; ++i)
    {
      b[i]=fRand(0.,1.);
      c[i]=fRand(0.,1.);
    }

  for (unsigned int e=0; e < Nelm; ++e)
    for (unsigned int i=0; i < N; ++i)
      for (unsigned int j=0; j < N; ++j)
        {
          a[e][i][j]=0.;
        }

  double t1=mytimer();

  for (unsigned int e=0; e < Nelm; ++e)
    for (unsigned int i=0; i < N; ++i)
      for (unsigned int j=0; j < N; ++j)
        {
          a[e][i][j]+=b[i]*c[j];
        }

  double t2=mytimer();

  cout << __func__ << " " << t2-t1 << endl;

  return a[0][entry][entry];

}

double single_linear(unsigned int entry)
{
  double *a, *b, *c;

  a=(double*)malloc(Nelm*N*N*sizeof(double));
  b=(double*)malloc(N*sizeof(double));
  c=(double*)malloc(N*sizeof(double));

  unsigned int S=Nelm*N*N;

  for (unsigned int i=0; i < N; ++i)
    {
      b[i]=fRand(0.,1.);
      c[i]=fRand(0.,1.);
    }

  for (unsigned int i=0; i < S; ++i)
    {
      a[i]=0.;
    }

  double t1=mytimer();

  for (unsigned int i=0; i < S; ++i)
    {
      a[i]+=b[i%N]*c[i%N];
    }

  double t2=mytimer();

  cout << __func__ << " " << t2-t1 << endl;

  double res=a[entry*entry];

  free(a);
  free(b);
  free(c);
  
  return res;

}

double c_double_pointer(unsigned int entry)
{
  double ***a, *b, *c;
  a=(double***)malloc(Nelm*sizeof(double **));
  b=(double*)malloc(N*sizeof(double));
  c=(double*)malloc(N*sizeof(double));


  for (unsigned int i=0; i < N; ++i)
    {
      b[i]=fRand(0.,1.);
      c[i]=fRand(0.,1.);
    }

  for (unsigned int e=0; e < Nelm; e++)
    {
      a[e]=(double**)malloc(N*sizeof(double*));
         for (unsigned int i=0; i < N; ++i)
        {
          a[e][i]=(double*)malloc(N*sizeof(double));
          for (unsigned int j=0; j < N; ++j)
            {
              a[e][i][j]=0.;
            }
                         
        }
    }
  
  double t1=mytimer();
  
  for (unsigned int e=0; e < Nelm; ++e)
    for (unsigned int i=0; i < N; ++i)
      for (unsigned int j=0; j < N; ++j)
        {
          a[e][i][j]+=b[i]*c[j];
        }

  double t2=mytimer();

  cout << __func__ << " " << t2-t1 << endl;

  double res=a[0][entry][entry];
  
  for (unsigned int e=0; e < Nelm; ++e)
    {
      for (unsigned int i=0; i < N; ++i)
        {
          free(a[e][i]);
        }
      free(a[e]);
    }
  free(a);
  free(b);
  free(c);

  return res;
    
}

double c_double_pointer_contig(unsigned int entry)
{
  double ***a, *b, *c;
  double *tmp=(double*)malloc(Nelm*N*N*sizeof(double));
  a=(double***)malloc(Nelm*sizeof(double **));
  b=(double*)malloc(N*sizeof(double));
  c=(double*)malloc(N*sizeof(double));


  for (unsigned int i=0; i < N; ++i)
    {
      b[i]=fRand(0.,1.);
      c[i]=fRand(0.,1.);
    }

  for (unsigned int e=0; e < Nelm; e++)
    {
      a[e]=(double**)malloc(N*sizeof(double*));
         for (unsigned int i=0; i < N; ++i)
        {
          a[e][i]=&tmp[e*N*N+i*N];
          for (unsigned int j=0; j < N; ++j)
            {
              a[e][i][j]=0.;
            }
                         
        }
    }
  
  double t1=mytimer();
  
  for (unsigned int e=0; e < Nelm; ++e)
    for (unsigned int i=0; i < N; ++i)
      for (unsigned int j=0; j < N; ++j)
        {
          a[e][i][j]+=b[i]*c[j];
        }

  double t2=mytimer();

  cout << __func__ << " " << t2-t1 << endl;

  double res=a[0][entry][entry];
  
  for (unsigned int e=0; e < Nelm; ++e)
    {
      free(a[e]);
    }
  free(a);
  free(b);
  free(c);
  free(tmp);

  return res;
    
}

double cpp_vector3(unsigned int entry)
{
  vector<vector<vector<double> > > a;
  vector<double> b,c;
  a.resize(Nelm);
  b.resize(N);
  c.resize(N);

  for (unsigned int i=0; i < N; ++i)
    {
      b[i]=fRand(0.,1.);
      c[i]=fRand(0.,1.);
    }

  for (unsigned int e=0; e < Nelm; e++)
    {
      a[e].resize(N);
      for (unsigned int i=0; i < N; ++i)
        {
          a[e][i].resize(N);
          for (unsigned int j=0; j < N; ++j)
            {
              a[e][i][j]=0.;
            }
        }
    }

  double t1=mytimer();

  for (unsigned int e=0; e < Nelm; ++e)
    for (unsigned int i=0; i < N; ++i)
      {
        #pragma ivdep
        for (unsigned int j=0; j < N; ++j)
          {
            a[e][i][j]+=b[i]*c[j];
          }
      }

  double t2=mytimer();

  cout << __func__ << " " << t2-t1 << endl;

  return a[0][entry][entry];

}

#define A(e,i,j) a[(e)*N*N + (i)*N + (j)]
double c_poly(unsigned int entry)
{
  double *a, *b, *c;

  a=(double *)malloc(Nelm*N*N*sizeof(double));
  b=(double *)malloc(N*sizeof(double));
  c=(double *)malloc(N*sizeof(double));

  for (unsigned int i=0; i < N; ++i)
    {
      b[i]=fRand(0.,1.);
      c[i]=fRand(0.,1.);
    }

  for (unsigned int e=0; e < Nelm; e++)
    for (unsigned int i=0; i < N; ++i)
      for (unsigned int j=0; j < N; ++j)
        A(e,i,j)=0.;

  double t1=mytimer();

  for (unsigned int e=0; e < Nelm; ++e)
    for (unsigned int i=0; i < N; ++i)
      for (unsigned int j=0; j < N; ++j)
        A(e,i,j)+=b[i]*c[j];

  double t2=mytimer();

  cout << __func__ << " " << t2-t1 << endl;

  return A(0,entry,entry);

}
#undef A

#define A(e,i,j) a[(e)*N*N + (i)*N + (j)]
double cpp_vec_poly(unsigned int entry)
{
  vector<double> a, b, c;
  a.resize(Nelm*N*N);
  b.resize(N);
  c.resize(N);

  for (unsigned int i=0; i < N; ++i)
    {
      b[i]=fRand(0.,1.);
      c[i]=fRand(0.,1.);
    }

  for (unsigned int e=0; e < Nelm; e++)
    for (unsigned int i=0; i < N; ++i)
      for (unsigned int j=0; j < N; ++j)
        A(e,i,j)=0.;

  double t1=mytimer();

  for (unsigned int e=0; e < Nelm; ++e)
    for (unsigned int i=0; i < N; ++i)
      for (unsigned int j=0; j < N; ++j)
        A(e,i,j)+=b[i]*c[j];

  double t2=mytimer();

  cout << __func__ << " " << t2-t1 << endl;

  return A(0,entry,entry);

}
#undef A

double boost_multi_array(unsigned int entry)
{
  typedef multi_array<double, 3> array_type_3;
  typedef array_type_3::index index;
  typedef multi_array<double, 1> array_type_1;

  array_type_3 a(extents[Nelm][N][N]);
  array_type_1 b(extents[N]);
  array_type_1 c(extents[N]);
  
  for (index i=0; i < N; ++i)
    {
      b[i]=fRand(0.,1.);
      c[i]=fRand(0.,1.);
    }

  for (index e=0; e < Nelm; e++)
    for (index i=0; i < N; ++i)
      for (index j=0; j < N; ++j)
        a[e][i][j]=0.f;

  double t1=mytimer();

  for (index e=0; e < Nelm; e++)
    for (index i=0; i < N; ++i)
      for (index j=0; j < N; ++j)
        a[e][i][j]+=b[i]*c[j];

  double t2=mytimer();

  cout << __func__ << " " << t2-t1 << endl;

  return a[0][entry][entry];

}


int main(int argc, char* argv[])
{
  if (argc <3)
    {
      N=100;
      Nelm=10000;
    }
  else
    {
      N=atol(argv[1]);
      Nelm=atol(argv[2]);
    }
  
  cout << "Nominal Memory Use: " << (Nelm*N*N*8+2*N*8)/(1024*1024) << "MiB"
       << endl;
  double res=0.;
  res+=c99_triple(10);
  res+=c_double_pointer(10);
  res+=c_double_pointer_contig(10);
  res+=cpp_vector3(10);
  res+=c_poly(10);
  res+=cpp_vec_poly(10);
  res+=boost_multi_array(10);
  res+=single_linear(10);
  cout << res << endl;
  return 0;
}
