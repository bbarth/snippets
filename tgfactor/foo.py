#!/usr/bin/env python

import urllib
import xml.etree.ElementTree as ET
import datetime


def main():

  data={}
  tree = ET.parse(urllib.urlopen('http://info.teragrid.org/web-apps/xml/kit-rdr-v3/'))
  root = tree.getroot()
  for comp in root.iter('{http://mds.teragrid.org/2007/02/ctss}RDR_Compute'):
    comp_resource_name=comp.find('{http://mds.teragrid.org/2007/02/ctss}ResourceID').text
    # Now that we've got the name of a compute resource, go find its Resource
    # record and its decommissioning date

    for resource in root.iter('{http://mds.teragrid.org/2007/02/ctss}RDR_Resource'):
      resource_name=resource.find('{http://mds.teragrid.org/2007/02/ctss}ResourceID').text
      if comp_resource_name == resource_name:
        avail=resource.find('.//{http://mds.teragrid.org/2007/02/ctss}IsPubliclyAvailable').text
        for status in resource.findall('.//{http://mds.teragrid.org/2007/02/ctss}ResourceStatus'):
          status_type=status.find('{http://mds.teragrid.org/2007/02/ctss}ResourceStatusType')
          if status_type.text=='Decommissioned':
            decom_date_str=status.find('{http://mds.teragrid.org/2007/02/ctss}StartDate').text
            decom_date=datetime.datetime.strptime(decom_date_str,'%Y-%m-%d')

    # See if it has a factor and the machine is
    # still running, print the machine name and the factor.
    # Need to add a search for Forge and Steele here
    try:
      for factor in comp.findall('.//{http://mds.teragrid.org/2007/02/ctss}ConversionFactor'):
        ftype = factor.find('.//{http://mds.teragrid.org/2007/02/ctss}ConvertTo')
        if ftype.text != 'Teragrid':
          continue
        x=factor.find('.//{http://mds.teragrid.org/2007/02/ctss}Factor')
    except AttributeError:
      continue

    comp_resource_name=comp.find('{http://mds.teragrid.org/2007/02/ctss}ResourceID').text
    if (not x is None and decom_date > datetime.datetime.today() and \
        avail == 'true') or (comp_resource_name in ['forge.ncsa.teragrid.org',
                                                    'steele.purdue.teragrid.org']):
      data[comp_resource_name]=float(x.text)


  for kv in sorted(data.items(), key=lambda x: x[1]):
    print kv[0], kv[1]
  

if __name__ == '__main__':
  main()
