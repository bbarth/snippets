load foo.dat

plot(foo(:,1),foo(:,2),'r-', ...
     foo(:,1),foo(:,3),'g-', ...
     foo(:,1),foo(:,4),'b-', ...
     foo(:,1),foo(:,5),'c-')

legend('matmul','dgemv','wrapped matmul','naive sub')
xlabel('N')
ylabel('MFlops')

print -dpng -f1 res.png

xm=25

xlim ([0 xm])
ylim ([0 max(foo(xm,2:5))*1.02])

print -dpng -f1 res2.png

clf

plot(foo(:,1),foo(:,3)./foo(:,2))

legend('dgemv/matmul')
xlabel('N')
ylabel('Ratio')

print -dpng -f1 res3.png

