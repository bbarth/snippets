program main

double precision, allocatable, dimension(:,:) :: a,b

allocate(a(10,2),b(10,2))
call random_number(a)
call random_number(b)

print *, a(:,1)*b(:,2)

end program main
