module wrapper

contains
  !DEC$ ATTRIBUTES NOINLINE :: mywrap
  subroutine mywrap(a,x,y)
    
    double precision, dimension(:,:), intent(in) :: a
    double precision, dimension(:),  intent(in) :: x
    double precision, dimension(:),  intent(inout) :: y

    y = matmul(a,x)
  end subroutine mywrap

  !DEC$ ATTRIBUTES NOINLINE :: naive
  subroutine naive(a,x,y,n)
    
    double precision, dimension(:,:),  intent(in) :: a
    double precision, dimension(:),  intent(in) :: x
    double precision, dimension(:),  intent(inout) :: y
    integer :: i, n

    do i=1,n
       do j=1,n
          y(i)=a(i,j)*x(j)
       enddo
    enddo

  end subroutine naive

end module

! subroutine mywrap(a,x,y)
! 
! double precision, dimension(:,:), allocatable, intent(in) :: a
! double precision, dimension(:), allocatable, intent(in) :: x
! double precision, dimension(:), allocatable, intent(inout) :: y
! 
! y = matmul(a,x)
! 
! end subroutine

program main

use wrapper

implicit none

double precision, dimension(:,:,:), allocatable :: a
double precision, dimension(:,:), allocatable :: x,y
double precision :: oned, zerod

integer(kind=4) :: i, itermax, nmax, n
integer(kind=8) :: t1, t2, t3, t4, t5, count_rate
double precision :: f1, f2, f3, f4
integer :: onei, zeroi, elements, e

elements=101
nmax=300
oned=1.d0
zerod=0.d0
onei=1
zeroi=0

do n=1,nmax
   itermax=max(100,min(10000,5*int(1000000.d0/dble(n*n)))) ! try to put reasonable bounds on the number of iterations
   allocate(a(n,n,elements),x(n,elements),y(n,elements))

   call random_number(a)
!   x=1.d0
   call random_number(x)
   y=x

!  warm up the MKL dgemv

   call dgemv('N', n, n, oned, a, n, x, onei, zerod, y, onei)

   call system_clock(t1,count_rate)
   do i=1,itermax
      e=mod(i-1,elements)+1
      y(:,e)=matmul(a(:,:,e),x(:,e))
   enddo
   call system_clock(t2,count_rate)

   do i=1,itermax
      e=mod(i-1,elements)+1
      call dgemv('N', n, n, oned, a(:,:,e), n, x(:,e), &
           & onei, zerod, y(:,e), onei)
   enddo
   call system_clock(t3,count_rate)

   do i=1,itermax
      e=mod(i-1,elements)+1
      call mywrap(a(:,:,e),x(:,e),y(:,e))
   enddo
   call system_clock(t4,count_rate)

   do i=1,itermax
      e=mod(i-1,elements)+1
      call naive(a(:,:,e),x(:,e),y(:,e),n)
   enddo
   call system_clock(t5,count_rate)

   f1=dble(n*(2*n-1))/dble(itermax)/(dble(t2-t1)/dble(count_rate))*1.d-6
   f2=dble(n*(2*n-1))/dble(itermax)/(dble(t3-t2)/dble(count_rate))*1.d-6
   f3=dble(n*(2*n-1))/dble(itermax)/(dble(t4-t3)/dble(count_rate))*1.d-6
   f4=dble(n*(2*n-1))/dble(itermax)/(dble(t5-t4)/dble(count_rate))*1.d-6

   write (*,'(I, E15.7, E15.7, E15.7, E15.7, I)') n, f1, f2, f3, f4, itermax
   deallocate (a,x,y)
enddo

end program
