#!/bin/bash

# Initialize Lmod

. /opt/apps/lmod/lmod/init/bash >/dev/null # Module Support

systemMPATH="/opt/apps/xsede/modulefiles /opt/apps/modulefiles /opt/modulefiles"
if [ "$TACC_SYSTEM" = hikari ]; then
  systemMPATH="/opt/apps/modulefiles /opt/modulefiles"
fi 
   
MODULEPATH=`/opt/apps/lmod/lmod/libexec/addto --append MODULEPATH $systemMPATH`
export MODULEPATH

# Load MATLAB module

module load matlab

# Check the license file to see if the server is up 

$TACC_MATLAB_DIR/etc/glnxa64/lmutil lmstat -c $LM_LICENSE_FILE

