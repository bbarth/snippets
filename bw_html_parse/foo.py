#!/usr/bin/env python
# -*- python -*-
from __future__ import print_function
import os, sys, re

import HTMLParser

class MyHTMLParser(HTMLParser.HTMLParser):
  found_PI=False
  
  def handle_starttag(self, tag, attrs):
#    if((tag == 'a')):
    try:
      if(attrs):
        r=re.match('.*pidetail.*pi=([a-zA-Z0-9]+)',attrs[0][1])
#        print(attrs[0][1],r)

        if((tag == 'a') and (r)):
          # print("PI start: ",r.group(1))
          self.found_PI=True
    except:
      pass

###   def handle_endtag(self, tag):
###     if (tag == 'a'):
###       print("Encountered an end tag :", tag)

  def handle_data(self, data):
    if (self.found_PI):
      print(data)
      self.found_PI=False

def main():
  s=''
  with open(sys.argv[1],"r") as f:
    s+=f.read()


  parser=MyHTMLParser()

  parser.feed(s)

#  print(parser)
    
  

if ( __name__ == '__main__'):
  main()

