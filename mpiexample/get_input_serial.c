#include <stdio.h>
#include <stdlib.h>

#include "serial_util.h"
#include "serial_proto.h"

void get_input(int argc, char** argv, struct Params *p)
{
  char tmp[1024];
  int ierr=0;
    
  
  FILE *in;
  if(argc==2)
    {
      in=fopen(argv[1],"r");
    }
  else
    {
      printf("Usage: %s input_file\n",argv[0]);
      exit(11);
    }

  // Extract values from file
  fscanf(in,"%Ld %Ld %lf %lf %d",
         &p->NX, &p->NY, &p->k, &p->dt, &p->ntsteps);

  if (p->ntsteps < 0 || p->dt < 0. || p->k < 0.)
    {
      printf("All parameters must be positive. ntsteps: %d, dt: %g, k: %g\n",
             p->ntsteps,p->dt,p->k);
      exit(13);
    }

  // Compute number of interior points
  
  p->nx=(p->NX-2);
  p->ny=(p->NY-2);

  if(p->nx<=0 || p->ny<=0)
    {
      printf("Must have at least one real node; nx=%Ld,ny=%Ld\n",p->nx,p->ny);
      exit(15);
    }

  // Compute grid spacing and check validity

  p->dx=1./((double) (p->NX-1));
  p->dy=1./((double) (p->NY-1));
  
  if (p->dx!=p->dy)
    {
      printf("Must use equal grid spacing. Got %gx%g\n",p->dx,p->dy);
      exit(16);
    }

}
