#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "serial_util.h"
#include "serial_proto.h"

int main(int argc, char** argv)
{
  int ierr=-1; // MPI error return variable (used by CHECKMPI)
  struct Params p={-1,-1,-1,-1,-1,-1, // the ints
                   -1.,-1.,-1.,-1.};  // the doubles

  get_input(argc, argv, &p); // Process input 
  
  // Allocate needed buffers
  
  double *u[2]={NULL,NULL};      // solutions
  double *f=NULL;                // rhs
  double *uold=NULL, *unew=NULL; // convenience pointers

  ALLOCATE(u[0],sizeof(double)*(p.nx+2)*(p.ny+2));
  ALLOCATE(u[1],sizeof(double)*(p.nx+2)*(p.ny+2));
  ALLOCATE(f,   sizeof(double)*(p.nx+2)*(p.ny+2)); 

  uold = u[0]; 
  unew = u[1];

  // Apply initial and boundary conditions; set RHS vector
  for(long long i=0; i<p.nx+2; ++i)
    for(long long j=0; j<p.ny+2; ++j)
      {
        double x=i*p.dx;
        double y=j*p.dy;
        UOLD(i,j) = 0.;
        UNEW(i,j) = 0.;
        F(i,j)    = 2.*M_PI*M_PI*p.k*sin(M_PI*x)*sin(M_PI*y);
      }

  double tstart=mytimer();

  double dtk_dx2=(p.dt*p.k/(p.dx*p.dx));
  
  for(int tstep=0; tstep < p.ntsteps; ++tstep)
    {
      // Swap uold and unew
      uold=u[tstep%2];
      unew=u[(tstep+1)%2];

      // Compute updated unew
      for(long long i=1; i<p.nx+1; ++i)
        for(long long j=1; j<p.ny+1; ++j)
          UNEW(i,j)=(UOLD(i,j) +
                     dtk_dx2*(-4*UOLD(i,j) +
                              UOLD(i-1,j) + UOLD(i+1,j) +
                              UOLD(i,j-1) + UOLD(i,j+1)) +
                     p.dt*F(i,j));

    }

  double tend=mytimer();
  double diff=tend-tstart;

  printf("%% %s mode run: %15.7g seconds, %5d processors\n",
         "serial",diff,1);

  compute_error(uold,unew,p);
  
  //write_ascii(p,unew);

  free(u[0]);
  free(u[1]);
  free(f);
  
  return(ierr);
}
