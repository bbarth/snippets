#include <stdio.h>
#include <math.h>

#include "serial_util.h"
#include "serial_proto.h"

void compute_error(double *uold, double *unew, struct Params p)
{
  // Check answer
  double tdiff=0.;    // unew - uold
  double ediff=0.;    // unew - uexact
  for(long long j=p.ny; j>0; --j)
    for(long long i=1; i<p.nx+1; ++i)
      {
        double x=i*p.dx;
        double y=j*p.dy;
        double v=sin(M_PI*x)*sin(M_PI*y)*
          (1.-exp(-2*M_PI*M_PI*p.k*((double)p.ntsteps-1)*p.dt));
        double temp=UNEW(i,j)-v;
        tdiff+=temp*temp;
        double temp2=UNEW(i,j)-UOLD(i,j);
        ediff+=temp2*temp2;
      }

  printf("%% %g %g\n", sqrt(ediff/(p.NX*p.NY)),sqrt(tdiff/(p.NX*p.NY)));

}
