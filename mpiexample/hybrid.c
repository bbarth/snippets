#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define _GNU_SOURCE 1 
#include <sched.h>
#include <spawn.h>
#include "mpi.h"
#include "omp.h"
#include "util.h"
#include "proto.h"
#include "comm_proto.h"

int main(int argc, char** argv)
{
  int max_threads=omp_get_max_threads();
  int ierr=-1; // MPI error return variable (used by CHECKMPI)
  struct Params p ={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, // the ints
                    -1.,-1.,-1.,-1.};                    // the doubles

  CHECKMPI(MPI_Init(&argc, &argv));
  CHECKMPI(MPI_Comm_size(MCW, &p.np));
  CHECKMPI(MPI_Comm_rank(MCW, &p.i_am));

  if (p.i_am==0)
    {
      printf("%% Started up on all procs\n");
      fflush(stdout);
    }

  get_input(argc, argv, &p); // Process input 
  
  // Allocate needed buffers
  
  double *u[2]={NULL,NULL};      // solutions
  double *f=NULL;                // rhs
  double *uold=NULL, *unew=NULL; // convenience pointers

  ALLOCATE(u[0],sizeof(double)*(p.nx+2)*(p.ny+2));
  ALLOCATE(u[1],sizeof(double)*(p.nx+2)*(p.ny+2));
  ALLOCATE(f,   sizeof(double)*(p.nx+2)*(p.ny+2)); 

  uold = u[0]; 
  unew = u[1];

  // Compute processor grid position

  p.ip=p.i_am%p.NPX;
  p.jp=p.i_am/p.NPX;

  COMM_INIT(CMODE,p); // Initialize neighbor info and allocate

  // Apply initial and boundary conditions; set RHS vector

#pragma omp parallel
 {
//   cpu_set_t  mask;
// 
//   CPU_ZERO(&mask);
//   CPU_SET(omp_get_thread_num(), &mask);
//   sched_setaffinity( (pid_t) 0 , sizeof(mask), &mask ) ; 

#pragma omp for
  for(long long i=0; i<p.nx+2; ++i)
    for(long long j=0; j<p.ny+2; ++j)
      {
        double x=p.ip*(p.nx)*p.dx + i*p.dx;
        double y=p.jp*(p.ny)*p.dy + j*p.dy;
        UOLD(i,j) = 0.;
        UNEW(i,j) = 0.;
        F(i,j)    = 2.*M_PI*M_PI*p.k*sin(M_PI*x)*sin(M_PI*y);
      }
 }

  double tstart=mytimer();

  double dtk_dx2=(p.dt*p.k/(p.dx*p.dx));
#pragma omp parallel
{
  
  for(int tstep=0; tstep < p.ntsteps; ++tstep)
    {
#pragma omp single
      {
      // Swap uold and unew
      uold=u[tstep%2];
      unew=u[(tstep+1)%2];
      }
      
      // Compute updated unew

#pragma omp for 
      for(long long i=1; i<p.nx+1; ++i)
        {
          for(long long j=1; j<p.ny+1; ++j)
            {
              UNEW(i,j)=(UOLD(i,j) +
                         dtk_dx2*
                         (-4.*UOLD(i,j) +
                          UOLD(i-1,j) + UOLD(i+1,j)  +
                          UOLD(i,j-1) + UOLD(i,j+1)) +
                         p.dt*F(i,j));
            }
        }

#pragma omp single
      {
      COMM(CMODE,unew,p); // Exchange ghost data
      }

    }
}  

  double tend=mytimer();
  double diff=tend-tstart;
  double gdiff=0.;

  CHECKMPI(MPI_Reduce(&diff,&gdiff,1,MPI_DOUBLE,MPI_SUM,0,MCW));

  if(p.i_am==0)
    printf("%% %s mode run: %15.7g seconds, %5d processors, %2d threads\n",
           XSTR(CMODE),gdiff/p.np,p.np,max_threads);

  compute_error(uold,unew,p);
  
  //  write_ascii(p,unew);

  COMM_CLEANUP(CMODE); // Deallocate comm buffers, etc.

  free(u[0]);
  free(u[1]);
  free(f);
  
  ierr=MPI_Finalize(); // Checking MPI_Finalize for errors not required
  return(ierr);
}
