#include <stdio.h>
#include <math.h>
#include "mpi.h"
#include "util.h"
#include "proto.h"

void compute_error(double *uold, double *unew, struct Params p)
{
  int ierr=-1;
  // Check answer
  double tdiff=0.;    // unew - uold
  double ediff=0.;    // unew - uexact
  for(long long j=p.ny; j>0; --j)
    for(long long i=1; i<p.nx+1; ++i)
      {
        double x=p.ip*(p.nx)*p.dx + i*p.dx;
        double y=p.jp*(p.ny)*p.dy + j*p.dy;
        double v=sin(M_PI*x)*sin(M_PI*y)*
          (1.-exp(-2*M_PI*M_PI*p.k*((double)p.ntsteps-1)*p.dt));
        double temp=UNEW(i,j)-v;
        tdiff+=temp*temp;
        double temp2=UNEW(i,j)-UOLD(i,j);
        ediff+=temp2*temp2;
      }

  double gtdiff=0., gediff=0.;

  // Sum up globally
  CHECKMPI(MPI_Reduce(&tdiff,&gtdiff,1,MPI_DOUBLE,MPI_SUM,0,MCW));
  CHECKMPI(MPI_Reduce(&ediff,&gediff,1,MPI_DOUBLE,MPI_SUM,0,MCW));

  if(p.i_am==0)
    printf("%% %g %g\n", sqrt(gediff/(p.NX*p.NY)),sqrt(gtdiff/(p.NX*p.NY)));

}
