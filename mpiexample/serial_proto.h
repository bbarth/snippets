#include "serial_util.h"

void get_input(int argc, char** argv, struct Params *p);
void compute_error(double *uold, double *unew, struct Params p);
void write_ascii(struct Params p, double *unew);
double mytimer();
