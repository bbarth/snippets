#ifndef _PCSE_UTIL_H
#define _PCSE_UTIL_H

#ifndef M_PI
#define M_PI		3.14159265358979323846L
#endif

#define UOLD(i,j) uold[(i)*(p.ny+2)+(j)]
#define UNEW(i,j) unew[(i)*(p.ny+2)+(j)]
#define F(i,j) f[(i)*(p.ny+2)+(j)]

#define HERE printf("%s: %d\n",__FILE__,__LINE__)

#define ALLOCATE(x,y) do { (x)=malloc(y);                            \
                        if ((x)==NULL) {                             \
                          printf("Failed to allocate: %Ld: %s:%d\n", \
                                 (y),__FILE__,__LINE__);             \
                          exit(2);} } while(0)

enum { N=0, S, E, W};

struct Params
{
  long long NX, NY, nx, ny;
  int nsteps, ntsteps;
  double dt, k, dx, dy;
};

#endif
