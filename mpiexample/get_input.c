#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"
#include "util.h"
#include "proto.h"

void get_input(int argc, char** argv, struct Params *p)
{
  // Read input file processor 0 and broadcast to all
  char tmp[1024];
  int ierr=0;
    
  if (p->i_am==0)
    {
      FILE *in;
      if(argc==2)
        {
          in=fopen(argv[1],"r");
        }
      else
        {
          printf("Usage: %s input_file\n",argv[0]);
          MPI_Abort(MCW,11);
          exit(1);
        }
      fscanf(in,"%[0-9. ]\n",tmp);
      fclose(in);
    }
  
  CHECKMPI(MPI_Bcast(&tmp[0],1024,MPI_BYTE,0,MCW));

  // Extract values from broadcast data
  sscanf(tmp,"%Ld %Ld %d %d %lf %lf %d",
         &p->NX, &p->NY, &p->NPX, &p->NPY, &p->k, &p->dt, &p->ntsteps);

  // Check input quality
  
  if(p->NPX*p->NPY!=p->np)
    {
      printf("Partition not compatible with communicator size: %dx%d!=%d\n",
             p->NPX,p->NPY,p->np);
      MPI_Abort(MCW,12);
    }
  
  if (p->ntsteps < 0 || p->dt < 0. || p->k < 0.)
    {
      printf("All parameters must be positive. ntsteps: %d, dt: %g, k: %g\n",
             p->ntsteps,p->dt,p->k);
      MPI_Abort(MCW,13);
    }

  // Compute number of interior points and check validity
  
  p->nx=(p->NX-2)/p->NPX;
  p->ny=(p->NY-2)/p->NPY;

  if((p->NX-2 != p->nx*p->NPX) || (p->NY-2 != p->ny*p->NPY))
    {
      printf("Processor grid does not evenly divide the interior nodes!"
             "\n %dx%d, %Ldx%Ld\n",p->NPX,p->NPY,p->NX-2,p->NY-2);
      MPI_Abort(MCW,14);
    }

  if(p->nx<=0 || p->ny<=0)
    {
      printf("Must have at least one real node; nx=%Ld,ny=%Ld\n",p->nx,p->ny);
      MPI_Abort(MCW,15);
    }

  // Compute grid spacing and check validity

  p->dx=1./((double) (p->NX-1));
  p->dy=1./((double) (p->NY-1));
  
  if (p->dx!=p->dy)
    {
      printf("Must use equal grid spacing. Got %gx%g\n",p->dx,p->dy);
      MPI_Abort(MCW,16);
    }

}
