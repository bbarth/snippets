#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "util.h"
#include "proto.h"

double *sbuf[4]={NULL,NULL,NULL,NULL};
double *rbuf[4]={NULL,NULL,NULL,NULL};
int neigh[4]={-1,-1,-1,-1};

void comm_ssend_init(struct Params p)
{
  int ierr=-1;
  // Allocate bufs
  
  CALLOCATE(sbuf[N],sizeof(double)*(p.nx+2)); 
  CALLOCATE(sbuf[S],sizeof(double)*(p.nx+2)); 
  CALLOCATE(sbuf[E],sizeof(double)*(p.ny+2)); 
  CALLOCATE(sbuf[W],sizeof(double)*(p.ny+2)); 
  CALLOCATE(rbuf[N],sizeof(double)*(p.nx+2)); 
  CALLOCATE(rbuf[S],sizeof(double)*(p.nx+2)); 
  CALLOCATE(rbuf[E],sizeof(double)*(p.ny+2)); 
  CALLOCATE(rbuf[W],sizeof(double)*(p.ny+2)); 

  // Compute neighbor data

  neigh[N]=p.NPX*(p.jp+1)+p.ip;
  neigh[S]=p.NPX*(p.jp-1)+p.ip;
  neigh[E]=p.i_am+1;
  neigh[W]=p.i_am-1;

  if (p.ip==0) neigh[W]=MPI_PROC_NULL;
  if (p.ip==p.NPX-1) neigh[E]=MPI_PROC_NULL;
  if (p.jp==0) neigh[S]=MPI_PROC_NULL;
  if (p.jp==p.NPY-1) neigh[N]=MPI_PROC_NULL;


}

void comm_ssend(double *unew, struct Params p)
{
  int ierr=-1;
  // Copy Data to Send Buffers
  for (long long j=0; j<p.ny+2; ++j)
    {
      sbuf[E][j]=UNEW(p.nx,j);
      sbuf[W][j]=UNEW(1,j);
    }
  for (long long i=0; i<p.nx+2; ++i)
    {
      sbuf[N][i]=UNEW(i,p.ny);
      sbuf[S][i]=UNEW(i,1);
    }

  // Exchange Ghost Data using ssend to force no buffering
  MPI_Status status;

  CHECKMPI(MPI_Ssend(&sbuf[N][0],p.nx+2,MPI_DOUBLE,neigh[N],1,MCW));
  CHECKMPI(MPI_Recv(&rbuf[S][0],p.nx+2,MPI_DOUBLE,neigh[S],1,MCW,&status));

  CHECKMPI(MPI_Ssend(&sbuf[S][0],p.nx+2,MPI_DOUBLE,neigh[S],1,MCW));
  CHECKMPI(MPI_Recv(&rbuf[N][0],p.nx+2,MPI_DOUBLE,neigh[N],1,MCW,&status));

  CHECKMPI(MPI_Ssend(&sbuf[E][0],p.ny+2,MPI_DOUBLE,neigh[E],1,MCW));
  CHECKMPI(MPI_Recv(&rbuf[W][0],p.ny+2,MPI_DOUBLE,neigh[W],1,MCW,&status));

  CHECKMPI(MPI_Ssend(&sbuf[W][0],p.ny+2,MPI_DOUBLE,neigh[W],1,MCW));
  CHECKMPI(MPI_Recv(&rbuf[E][0],p.ny+2,MPI_DOUBLE,neigh[E],1,MCW,&status));

  // Copy Data From Receive Buffers
  for (long long j=0; j<p.ny+2; ++j)
    {
      UNEW(0,j)      = rbuf[W][j];
      UNEW(p.nx+1,j) = rbuf[E][j];
    }
  for (long long i=0; i<p.nx+2; ++i)
    {
      UNEW(i,0)      = rbuf[S][i];
      UNEW(i,p.ny+1) = rbuf[N][i];
    }
}

void comm_ssend_cleanup()
{
  free(sbuf[N]);
  free(sbuf[S]);
  free(sbuf[E]);
  free(sbuf[W]);
  free(rbuf[N]);
  free(rbuf[S]);
  free(rbuf[E]);
  free(rbuf[W]);
}
