#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"
#include "util.h"
#include "proto.h"

void write_ascii(struct Params p, double *unew)
{
  // I/O (Gather the whole grid and print it out from proc 0)

  double *wholegrid; // Allocate enough space to hold the whole mesh
  ALLOCATE(wholegrid,sizeof(double)*p.NX*p.NY); 
  double *gwholegrid;
  ALLOCATE(gwholegrid,sizeof(double)*p.NX*p.NY);

  // Find the local max and min in global numbers
  long long gimin=p.nx*p.ip+1;
  long long gimax=p.nx*(p.ip+1)+1;
  long long gjmin=p.ny*p.jp+1;
  long long gjmax=p.ny*(p.jp+1)+1;

  // Loop over the whole grid and insert values owned locally (zero
  // other entries, and the global sum)
  for (long long gi=0; gi<p.NX; ++gi)
    for (long long gj=0; gj<p.NY; ++gj)
      {
        if(gi>=gimin && gi<gimax && gj>=gjmin && gj<gjmax)
          {
            long long i=gi-gimin+1, j=gj-gjmin+1;
            wholegrid[gi*p.NY+gj]=UNEW(i,j);
          }
        else
          wholegrid[gi*p.NY+gj]=0.;
        gwholegrid[gi*p.NY+gj]=0.;
      }

  MPI_Reduce(&wholegrid[0],&gwholegrid[0],p.NX*p.NY,MPI_DOUBLE,MPI_SUM,0,MCW);

  // Print the mesh to the screen
  if (p.i_am==0)
    for (long long j=p.NY-1; j>-1; --j)
      {
        for(long long i=0; i<p.NX; ++i)
          printf("%9.5g ",gwholegrid[i*p.NY+j]);
        printf("\n");
      }
  free(wholegrid);
  free(gwholegrid);
}
