#include <stdio.h>
#include <stdlib.h>
#include "serial_util.h"
#include "serial_proto.h"

void write_ascii(struct Params p, double *unew)
{
  // Print the answers
  // rows in reverse order so that it matches our expectations
  for (long long j=p.NY-1; j>-1; --j)
    {
      for(long long i=0; i<p.NX; ++i)
        printf("%9.5g ",UNEW(i,j));
      printf("\n");
    }
}
