subroutine get_input(p)
  use util_serial
  implicit none

  integer*4 :: iarg
  character*256 :: varg
  type (Param) p

  ! Read input from command line

  if (iargc() .ne. 1) then
     call getarg(0,varg)
     write (*,100) 'Usage: ', varg, ' input_file'
     call exit(1)
  else
     call getarg(1,varg)
  endif

  open (10, FILE=varg)
  read(10,*), p%NX, p%NY, p%k, p%dt, p%ntsteps
  close(10)

  if (p%ntsteps < 0 .or.  p%dt < 0. .or.  p%k <0.) then
     write (*,*), 'All parameters must be positive. ntsteps: ', &
          p%ntsteps, 'dt: ', p%dt, 'k: ', p%k
     call exit(13)
  endif

  ! Compute number of interior points
  p%inx=p%NX-2
  p%iny=p%NY-2

  if (p%inx <= 0 .or. p%iny <= 0) then
     write(*,*), 'Must have at least one real node; ', &
          'inx=', p%inx, ' iny=', p%iny
     call exit(15)
  endif

  ! Compute grid spacing and check validity 
  p%dx=1./(p%NX-1.)
  p%dy=1./(p%NY-1.)

  if (p%dx .ne. p%dy) then
     write(*,*), 'Must use equal grid spacing. Got ', &
          p%dx,'x',p%dy
     call exit(16)
  endif


100 format (A,A10,A)

end subroutine get_input
