#define UNEW(i,j) u(i,j,p%flag)
#define UOLD(i,j) u(i,j,1-p%flag)
#define MCW MPI_COMM_WORLD

#define CHECKMPI(x) if ((x) .ne. 0) then ; print *,'Caught MPI error: ', (x), ' ', __FILE__,':',__LINE__ ; call MPI_Abort(MCW,1,ierr); endif

#define PASTE2(a,b) a ## b
#define PASTE3(a,b,c) a ## b ## c
#define XSTR(a) STR(a)
#define STR(a) #a

#define COMM_INIT(a,p) call PASTE3(comm_,a,_init)(p)
#define COMM(a,p) call PASTE2(comm_,a)(p)
#define COMM_CLEANUP(a) call PASTE3(comm_,a,_cleanup)()

#define HERE print*,__FILE__,':',__LINE__; call flush(6)


