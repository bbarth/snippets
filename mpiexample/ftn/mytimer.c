#include <sys/time.h>
#include <time.h>

double mytimer()
{
  struct timeval t;
  gettimeofday(&t,NULL);
  return ((double)t.tv_sec+1.e-6*(double)t.tv_usec);
}

double mytimer_()
{
  return mytimer();
}

double mytimer__()
{
  return mytimer();
}

double MYTIMER_()
{
  return mytimer();
}

double MYTIMER()
{
  return mytimer();
}

#include <signal.h>

void clear_signals_()
{
  signal(SIGSEGV,SIG_DFL);
}
