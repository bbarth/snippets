#include "util.h"

subroutine comm_sr_init(p)
  use util
  implicit none
  include 'mpif.h'

  integer*8 size

  type (Param) p

  ! Allocate send and receive buffers
  size=max(p%inx,p%iny)+1
  allocate(sbuf(0:size,4))
  allocate(rbuf(0:size,4))

  ! Compute neighbor information
  neigh(N)=p%NPX*(p%jp+1)+p%ip
  neigh(S)=p%NPX*(p%jp-1)+p%ip
  neigh(E)=p%i_am+1
  neigh(W)=p%i_am-1

  if (p%ip==0) neigh(W)=MPI_PROC_NULL
  if (p%ip==p%NPX-1) neigh(E)=MPI_PROC_NULL
  if (p%jp==0) neigh(S)=MPI_PROC_NULL
  if (p%jp==p%NPY-1) neigh(N)=MPI_PROC_NULL

end subroutine comm_sr_init

subroutine comm_sr(p)
  use util
  implicit none
  include 'mpif.h'

  type (Param) p

  integer :: status(MPI_STATUS_SIZE)
  integer*8 :: i,j

  ! Copy data to Send Buffers
  do j=0,p%iny+1
     sbuf(j,E)=UNEW(p%inx,j)
     sbuf(j,W)=UNEW(1,j)
  enddo
  do i=0,p%inx+1
     sbuf(i,N)=UNEW(i,p%iny)
     sbuf(i,S)=UNEW(i,1)
  enddo

  ! Shift Ghost Data
  call MPI_Sendrecv(sbuf(0,N),p%inx+2,MPI_DOUBLE_PRECISION,neigh(N),1, &
       rbuf(0,S),p%inx+2,MPI_DOUBLE_PRECISION,neigh(S),1, &
       MCW,status,ierr)
  CHECKMPI(ierr)

  call MPI_Sendrecv(sbuf(0,S),p%inx+2,MPI_DOUBLE_PRECISION,neigh(S),1, &
       rbuf(0,N),p%inx+2,MPI_DOUBLE_PRECISION,neigh(N),1, &
       MCW,status,ierr)
  CHECKMPI(ierr)

  call MPI_Sendrecv(sbuf(0,E),p%iny+2,MPI_DOUBLE_PRECISION,neigh(E),1, &
       rbuf(0,W),p%iny+2,MPI_DOUBLE_PRECISION,neigh(W),1, &
       MCW,status,ierr)
  CHECKMPI(ierr)

  call MPI_Sendrecv(sbuf(0,W),p%iny+2,MPI_DOUBLE_PRECISION,neigh(W),1, &
       rbuf(0,E),p%iny+2,MPI_DOUBLE_PRECISION,neigh(E),1, &
       MCW,status,ierr)
  CHECKMPI(ierr)

  ! Copy Data From Receive Buffers
  do j=0,p%iny+1
     UNEW(0,j)=rbuf(j,W)
     UNEW(p%inx+1,j)=rbuf(j,E)
  enddo
  do i=0,p%inx+1
     UNEW(i,0)=rbuf(i,S)
     UNEW(i,p%iny+1)=rbuf(i,N)
  enddo

end subroutine comm_sr

subroutine comm_sr_cleanup(p)
  use util
  implicit none
  type (Param) p

  deallocate(sbuf)
  deallocate(rbuf)

end subroutine comm_sr_cleanup
