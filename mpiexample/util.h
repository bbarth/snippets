#ifndef _PCSE_UTIL_H
#define _PCSE_UTIL_H

#ifndef M_PI
#define M_PI		3.14159265358979323846L
#endif

#define UOLD(i,j) uold[(i)*(p.ny+2)+(j)]
#define UNEW(i,j) unew[(i)*(p.ny+2)+(j)]
#define F(i,j) f[(i)*(p.ny+2)+(j)]

#define HERE printf("%s: %d\n",__FILE__,__LINE__)

#define MCW MPI_COMM_WORLD

#define CHECKMPI(x) do { ierr=(x);                              \
                      if (ierr!=0) {                            \
                        printf("Caught MPI error: %d: %s:%d\n", \
                               ierr,__FILE__,__LINE__);         \
                        MPI_Abort(MCW,1);} } while(0)

#define ALLOCATE(x,y) do { (x)=calloc(y,1);                           \
                        if ((x)==NULL) {                              \
                          printf("Failed to callocate: %Ld: %s:%d\n", \
                                 (y),__FILE__,__LINE__);              \
                          MPI_Abort(MCW,2);} } while(0)

#define CALLOCATE(x,y) do { (x)=malloc(y);                            \
                        if ((x)==NULL) {                             \
                          printf("Failed to allocate: %Ld: %s:%d\n", \
                                 (y),__FILE__,__LINE__);             \
                          MPI_Abort(MCW,2);} } while(0)

#define PASTE2(a,b) a ## b
#define PASTE3(a,b,c) a ## b ## c
#define XSTR(a) STR(a)
#define STR(a) #a

#define COMM_INIT(a,p) PASTE3(comm_,a,_init)(p)
#define COMM(a,u,p) PASTE2(comm_,a)(u,p)
#define COMM_CLEANUP(a) PASTE3(comm_,a,_cleanup)()


enum { N=0, S, E, W};

struct Params
{
  long long NX, NY, nx, ny;
  int NPX, NPY, ip, jp, nsteps, np, i_am, ntsteps;
  double dt, k, dx, dy;
};

#endif
