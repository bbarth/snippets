#!/bin/bash

rm *~ .*~
rm *.so
rm *.o

rm ./icc_main_gcc_init_*
rm ./gcc_main_gcc_init_*
rm ./ifort_main_gcc_init_*
rm ./gfortran_main_icc_init_*
rm ./gfortran_main_gcc_init_*
rm ./gcc_main_icc_init_*
rm ./icc_main_icc_init_*
rm ./ifort_main_icc_init_*

