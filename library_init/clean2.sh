#!/bin/bash

rm *~ .*~
rm *.so
rm *.o

rm ./icc_main_gcc_init
rm ./gcc_main_gcc_init
rm ./ifort_main_gcc_init
rm ./gfortran_main_icc_init
rm ./gfortran_main_gcc_init
rm ./gcc_main_icc_init
rm ./icc_main_icc_init
rm ./ifort_main_icc_init
rm ./gfortran_main
rm ./gcc_main
rm ./icc_main
rm ./ifort_main

