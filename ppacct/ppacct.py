#!/usr/bin/env python
# -*- python -*-
from __future__ import print_function
import os, sys, re, time, pwd

def main():
  print('')
  print('')
  for line in sys.stdin:
    (jobid,uid,acct,batch,start,end,submittime,queue,nothing,jobname,
       jobstate,nodes,procs)=line.rstrip().split(':')
    # print(jobid,uid,acct,batch,start,end,submittime,queue,nothing,jobname,jobstate,nodes,procs)
    uname=pwd.getpwuid(int(uid)).pw_name
    print('Job Id:', jobid, 'User: ',uname, 'Project: ',acct,
            '  Queue:',queue,' -N: ',nodes,' -n: ',procs)
    print('Started: %10s' % time.strftime('%Y-%m-%d %H:%M:%S',
    time.localtime(int(start))), end='   ' )
    print('Ended:   %10s' % time.strftime('%Y-%m-%d %H:%M:%S',
    time.localtime(int(end))), end='   ')
    print('Queued:  %10s' % time.strftime('%Y-%m-%d %H:%M:%S',
    time.localtime(int(submittime))))
    print('')

if ( __name__ == '__main__'):
  main()
