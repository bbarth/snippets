#include <omp.h>
int main()
{
  double sum; 
  int i,n, nt;
  
  n=2000;
  sum=1.0;
  
#pragma offload target(mic)
  {
#pragma omp parallel for reduction(+:sum)
    for(i=1;i<=n;i++)
      {
	sum += (double)i;
      }
    nt = omp_get_max_threads();
    printf("Hello MIC reduction %f threads: %d guess: %f\n",sum,nt,
	   ((double) n)/((double) nt)*(((double)n)/((double) nt)+1.)/2.);
  }
}
