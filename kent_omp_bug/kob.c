#include <omp.h>
int main()
{
  double sum; 
  int i,n, nt;
  
  n=2000;
  double *a=malloc(sizeof(double)*2001);
  
#pragma offload target(mic) inout(a:length(2001))
  {
#pragma omp parallel for 
    for(i=1;i<=n;i++)
      {
	a[i] = (double) i; 
      }
    nt = omp_get_max_threads();
    printf("num_threads: %d\n",nt);
  }
  for(i=1;i<=n;i++)
    {
      printf("%f ",a[i]);
    }
  
  printf("\n");

}
