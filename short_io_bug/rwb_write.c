#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

size_t min(size_t x, size_t y)
{
  return ((x < y) ? x : y);
}

size_t do_write(FILE *f, char *buf, size_t N, off_t max_size, size_t written_so_far)
{
  size_t bytes_written=0;
  size_t expected_write=min(N,max_size-written_so_far);
  printf("Expecting %d...",expected_write);
  bytes_written=write(fileno(f),&buf[0],N);
  if ( bytes_written && (bytes_written  != N) &&
       (bytes_written != expected_write) ) 
    {
      perror("Wrote less bytes, perror reports");
      fprintf(stderr,"Only wrote: %d\n",bytes_written);
      // Can I still write to this file?
      printf("\nWriting %d more just for fun...",N);
      bytes_written=write(fileno(f),&buf[0],N);
      if (bytes_written < 0)
        perror("Write failed, perror reports");
      printf("wrote: %d\n",bytes_written);
      exit(1);
    }
  else if(!bytes_written)
    {
      printf("Wrote 0 bytes, bad news!\n");
      exit(0);
    }
  else
    printf("got: %d...",bytes_written);

  return(bytes_written);
}

int main(int argc, char* argv[])
{
  
  FILE* f;
  if (argc==1)
    f=fopen("output.dat","w");
  else if (argc==2)
    f=fopen(argv[1],"w");
  else
    exit(3);

  size_t N=1024L*1024L*4+4;      // 4MiB + 4B to avoid alignment
  char *buf=(char *)malloc(N);
  size_t max_its=100;

  off_t file_size=max_its*N;
  printf("%zu expected file size",N);

  int cnt=0;
  size_t running_total=0;
  for (int iter=0; iter<max_its; ++iter)
    {
      for (int i=0; i < 85; ++i)
        {
          printf("Writing (%06d) ...",cnt++);
          running_total+=do_write(f,buf,N,file_size,running_total);
          printf("Done\n");
        }
    }
  
  
  return 0;
}
