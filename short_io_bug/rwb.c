#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

size_t min(size_t x, size_t y)
{
  return ((x < y) ? x : y);
}

size_t do_read(FILE *f, char *buf, size_t N, off_t max_size, size_t read_so_far)
{
  size_t bytes_read=0;
  size_t expected_read=min(N,max_size-read_so_far);
  printf("Expecting %d...",expected_read);
  bytes_read=read(fileno(f),&buf[0],N);
  if ( bytes_read && (bytes_read  != N) &&
       (bytes_read != expected_read) ) 
    {
      perror("Got less bytes, perror reports");
      fprintf(stderr,"Only read: %d\n",bytes_read);
      // Can I still read this file?
      printf("\nReading %d more just for fun...",N);
      bytes_read=read(fileno(f),&buf[0],N);
      printf("got: %d\n",bytes_read);
      exit(1);
    }
  else if(!bytes_read)
    {
      printf("Read 0 bytes, declaring end of file\n");
      exit(0);
    }
  else
    printf("got: %d...",bytes_read);

  return(bytes_read);
}

int main(int argc, char* argv[])
{
  
  FILE* f;
  if (argc==1)
    f=fopen("data.dat","r");
  else if (argc==2)
    f=fopen(argv[1],"r");
  else
    exit(3);

  size_t N=1024L*1024L*4+4;      // 4MiB + 4B to avoid alignment
  char *buf=(char *)malloc(N); 

  struct stat s;
  fstat(fileno(f),&s);
  off_t file_size=s.st_size;

  int cnt=0;
  size_t running_total=0;
  while(1) // Loop forever, the reader will exit on EOF
    {
      for (int i=0; i < 85; ++i)
        {
          printf("Reading (%06d) ...",cnt++);
          running_total+=do_read(f,buf,N,file_size,running_total);
          printf("Done\n");
        }
    }
  
  
  return 0;
}
