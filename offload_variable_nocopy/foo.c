#include <stdio.h>

__declspec(target(mic)) int x, y[100];

struct A
{
  int *p;
};

__declspec(target(mic)) struct A a;

void f()
{
  x = 55;
  // x sent from CPU, y computed on coprocessor
 
#pragma offload target(mic:0) in(x) nocopy(y,a)
  { y[50] = 66; a.p=&y[50]; }
 
#pragma offload target(mic:0) nocopy(x,y,a)
      { // x and y retain previous values }
	printf("%d %d %d\n",x,y[50], *(a.p)); fflush(0);
      }
}
