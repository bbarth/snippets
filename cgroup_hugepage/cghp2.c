#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>


int main()
{
  size_t pg_size=4096L;
  size_t hp_size=pg_size*1024L*1024L;

  ssize_t i=0;

  char fn[256]="/test";

  int fd=shm_open(fn,O_RDWR|O_CREAT,0600);
  perror("shm_open");

  ftruncate(fd,hp_size);

  char *buf=mmap(NULL,hp_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
  perror("mmap");
  close(fd);

  printf("%p\n",buf);
  for (i=0; i < hp_size; i+=pg_size)
    {
      buf[i]=i%256;
      printf("%zd\n",i);
    }

  sleep(1000);

  munmap(buf,hp_size);

  shm_unlink(fn);
  
  return(0);

}
