#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>


int main()
{
  size_t pg_size=4096L;
  size_t hp_size=pg_size*1024L*128L;

  ssize_t i=0;

  key_t key=(key_t)1234; // ftok("/dev/zero",12);

  perror("ftok");

  int id=shmget(key,hp_size,0644 | IPC_CREAT | SHM_HUGETLB);

  perror("shmget");

  printf("%d\n",id);

  char *buf=NULL;

  buf=shmat(id,(void *)0,0);
  perror("shmat");

  printf("%p\n",buf);
	    

  for (i=0; i < hp_size; i+=pg_size)
    buf[i]=i%256;

  sleep(1000);

  shmdt(buf);

  return(0);

}
