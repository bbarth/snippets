#include "mpi.h"
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char **argv, char **env)
{
  int num_procs, num_local;
  char mach_name[MPI_MAX_PROCESSOR_NAME];
  int mach_len=MPI_MAX_PROCESSOR_NAME-1;

  gethostname(mach_name, mach_len);
  
  printf ("In main, doing MPI_Init() on %s\n",mach_name);
  fflush(stdout);

  MPI_Init (&argc,&argv);
  MPI_Comm_size (MPI_COMM_WORLD, &num_procs);
  MPI_Comm_rank (MPI_COMM_WORLD, &num_local);
  MPI_Get_processor_name(mach_name,&mach_len);

  MPI_Barrier(MPI_COMM_WORLD);

  //  if(num_local == 0)
    {
      printf("\n Hello, world!\n");
      char *p;
      char cmd[1024];
      if (argc > 1)
	{
	  p=getenv(argv[1]);
	  printf("   -> Env. variable %s = %s\n",argv[1],p?p:"Not Set 0xdeadbeef");
	  sprintf(cmd,"/bin/ls -la %s\n",getenv(argv[1]));
	  printf("Running %s on %s\n",cmd,mach_name);
	  system(cmd);
	}
      else
	{
	  p=getenv("KOOMIE_TEST");
	  printf("   -> Env. variable KOOMIE_TEST = %s\n",p?p:"Not Set 0xdeadbeef");
	}
    }

  MPI_Barrier(MPI_COMM_WORLD);

  printf("    --> Process # %3i of %3i is alive. ->%s\n",
	 num_local,num_procs,mach_name);

  MPI_Finalize();


}
