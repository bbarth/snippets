#!/usr/bin/env python

def main():
  import subprocess
  import numpy as np
  import time

  b=1024*1024*256
  print 'allocating ', b*8

  a=np.ones((b,1))

  s=10

  print 'sleeping ', s

  time.sleep(s)

  print 'calling popen'

  p=subprocess.Popen(['sleep','10'])
  
  print 'calling wait'
  
  p.wait()

  print 'done'

if __name__ == '__main__':
  main()
  


