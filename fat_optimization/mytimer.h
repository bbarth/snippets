#include <sys/time.h>
#include <time.h>
#include <stdio.h>

#include "tacc_timers.h"

enum TimerType {GTOD=0, RDTSCP};

double mytimer_gtod();
double mytimer_rdtscp();

double mytimer(int which);
double mytimer_(int which);
double mytimer__(int which);
double MYTIMER_(int which);
double MYTIMER(int which);
