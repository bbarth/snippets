#define _POSIX_C_SOURCE 200112L
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "mytimer.h"

#include <errno.h>

#define BLK_SIZE 256
#define N 65536
#define INV_PI 0.318309886183791

void add(double *a, double *b, int start, int blk)
{
  for (int i=start; i<blk; ++i)
    a[i]+=a[i]+b[i];
}

void scale(double *a, double s, int start, int blk)
{
  for (int i=start; i<blk; ++i)
    a[i]*=s;
}

int main(int argc, char* argv[])
{
  double *A;
  double *B;
  
  if(posix_memalign((void**)&A,4096,N*sizeof(double)))
    abort();
  if(posix_memalign((void**)&B,4096,N*sizeof(double)))
    abort();

  for (int i=0; i<N; i+=BLK_SIZE)
    {
      A[i]=(double) i;
      B[i]=(double) (27.*i);
    }

  for (int i=0; i<N; i+=BLK_SIZE)
    {
      add(A,B,i,BLK_SIZE);
      scale(A,INV_PI,i,BLK_SIZE);
    }


  return A[1];
}
