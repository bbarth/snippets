#define _POSIX_C_SOURCE 200112L
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "mytimer.h"

#include <errno.h>

#define N 65536
#define INV_PI 0.318309886183791

void add(double *a, double *b, int i)
{
  a[i]+=a[i]+b[i];
}

void scale(double *a, double s, int i)
{
  a[i]*=s;
}

int main(int argc, char* argv[])
{
  double *A;
  double *B;
  
  if(posix_memalign((void**)&A,4096,N*sizeof(double)))
    abort();
  if(posix_memalign((void**)&B,4096,N*sizeof(double)))
    abort();

  for (int i=0; i<N; ++i)
    {
      A[i]=(double) i;
      B[i]=(double) (27.*i);
    }

  for (int i=0; i<N; ++i)
    {
      add(A,B,i);
      scale(A,INV_PI,i);
    }


  return A[1];
}
