#include <stdio.h>
#define N 1000
#define ITER_MAX 1000
int main(int argc, char* argv[])
{

  double a[N][N];
  double b[N], c[N];
  int i,j,it;

  for (i=0; i<N; ++i)
    {
      for (j=0; j<N; ++j)
        {
          a[i][j]=1.0/((double)(i+2*j+1));
        }
      c[i]=0.;
      b[i]=1.;
    }
      

  for (int it=0; it < ITER_MAX; ++it)
    {
      if (it%100 == 0) printf("%d\n",it);
#pragma omp parallel for
      for (i=0; i<N; ++i)
        {
          for (j=0; j<N; ++j)
            {
              c[i]+=a[i][j]*b[j];
            }
        }
    }

  printf("%g\n",a[N-1][N-2]);

  return 0;
}
