#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include "fftw3.h"
int main(int argc, char* argv[])
{
  int n=atoi(argv[1]);
  int i, cutoff = atoi(argv[2]);
  double *x=malloc(n*sizeof(double));
  for(i=0; i<n; ++i) x[i]=((double) rand())/((double)RAND_MAX+1.0);
  printf("x=[");
  for(i=0; i<n; ++i) printf("%15.7g ",x[i]); 
  printf("];\n\n");
  double complex *in, *out;
  fftw_plan p;
  in  = (double complex*) fftw_malloc(sizeof(double complex) * n);
  out = (double complex*) fftw_malloc(sizeof(double complex) * n);
  for(i=0; i<n; ++i) in[i]=(x[i]+0.*I);
  p = fftw_plan_dft_1d(n, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);
  fftw_destroy_plan(p);
  printf("y=[");
  for(i=0; i<n; ++i) 
    printf("%15.7g + %15.7gi ", 
           1./((double) n)*creal(out[i]),
	   1./((double) n)*cimag(out[i]));
  printf("];\n\n");
  out[0]=0.+0.*I;
  p = fftw_plan_dft_1d(n, out, in, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(p);
  printf("z=[");
  for(i=0; i<n; ++i) 
    printf("%15.7g + %15.7gi ",
	   1./((double) n)*creal(in[i]),
	   1./((double) n)*cimag(in[i]));
  printf("];\n\n");
  for(i=1; i<n/2+1; ++i)
    if (i > cutoff)
      out[i]=out[n-i]=0.+0.*I;
  printf("r=[");
  for(i=0; i<n; ++i) 
    printf("%15.7g + %15.7gi ",
	   1./((double) n)*creal(out[i]),
	   1./((double) n)*cimag(out[i]));
  printf("];\n\n");
  fftw_execute(p);
  printf("q=[");
  for(i=0; i<n; ++i)
    printf("%15.7g + %15.7gi ",
	   1./((double) n)*creal(in[i]),
	   1./((double) n)*cimag(in[i]));
  printf("];\n\n");
  fftw_destroy_plan(p);
  fftw_free(in);
  fftw_free(out); 
}
