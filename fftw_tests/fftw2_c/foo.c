#include <stdio.h>
#include <math.h>

#include "drfftw.h"

int main()
{
  int n = 32,i;
  double *x1, *x2;

  x1=(double*)malloc(sizeof(double)*n);
  x2=(double*)malloc(sizeof(double)*n);

  rfftw_plan plan, plan2;

  double twopi=atan(1.)*8.;

  printf("initial = ");
  for(i=0; i < n; i++)
    {
      x1[i]=sin(((double) i)*twopi/((double)(n-2)));
      printf("%g\t",x1[i]);
    }
  printf("\n");

  plan=rfftw_create_plan(n,FFTW_BACKWARD,FFTW_ESTIMATE);
  rfftw_one(plan,x1,x2);
  rfftw_destroy_plan(plan);

  printf("transformed = ");
  for(i=0; i < n; i++)
    {
        printf("%g\t",x2[i]);
	x1[i]=x2[i];
    }
  printf("\n");

  plan2=rfftw_create_plan(n,FFTW_FORWARD,FFTW_ESTIMATE);
  rfftw_one(plan2,x1,x2);
  rfftw_destroy_plan(plan2);
  
  printf("back = ");
  for(i=0; i < n; i++)
    {
      printf("%g\t",x2[i]/((double)(n)));
    }
  printf("\n");
  return 0;
}
