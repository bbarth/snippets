      program main

c Parameters for using FFTW in Fortran
c
c234567
c      
      integer FFTW_FORWARD,FFTW_BACKWARD
      parameter (FFTW_FORWARD=-1,FFTW_BACKWARD=1)
c
      integer FFTW_REAL_TO_COMPLEX,FFTW_COMPLEX_TO_REAL
      parameter (FFTW_REAL_TO_COMPLEX=-1,FFTW_COMPLEX_TO_REAL=1)
c
      integer FFTW_ESTIMATE,FFTW_MEASURE
      parameter (FFTW_ESTIMATE=0,FFTW_MEASURE=1)
c
      integer FFTW_OUT_OF_PLACE,FFTW_IN_PLACE,FFTW_USE_WISDOM
      parameter (FFTW_OUT_OF_PLACE=0)
      parameter (FFTW_IN_PLACE=8,FFTW_USE_WISDOM=16)
c
      integer FFTW_THREADSAFE
      parameter (FFTW_THREADSAFE=128)

      parameter (n=32)
      real*8 :: x1(n), x2(n)
      integer*8 plan, plan2
      twopi=atan(1.)*8.
      do i=1,n
         x1(i)=sin((i-1.)*twopi/(n-2.)) 
      enddo
      print *, 'initial     = ',x1
      call rfftw_f77_create_plan(plan,n,FFTW_BACKWARD,FFTW_ESTIMATE)
      call rfftw_f77_one(plan,x1,x2)
      call rfftw_f77_destroy_plan(plan)
      print *, 'transformed = ',x2
      
      x1=x2
      call rfftw_f77_create_plan(plan2,n,FFTW_FORWARD,FFTW_ESTIMATE)
      call rfftw_f77_one(plan2,x1,x2)
      call rfftw_f77_destroy_plan(plan2)
      print *, 'back        = ',x2/32.
      end
