subroutine foo(g,x,y,z)

include 'SIZE'

real*8 g(n2,d),x(n2),y(n2),z(n2)
real*8 t1,t2,t3

!dir$ vector aligned
do i=1,n2

   t1=g(i,1)*x(i) + g(i,2)*y(i) + g(i,3)*z(i)
   t2=g(i,2)*x(i) + g(i,4)*y(i) + g(i,5)*z(i)
   t3=g(i,3)*x(i) + g(i,5)*y(i) + g(i,6)*z(i)

   x(i)=t1
   y(i)=t2
   z(i)=t3

enddo

return
end

