!*VEC main.f90
!*VEC                                                                    Source Locations
!*VEC Message                                                                 Count     %
!*VEC streaming store was generated for main.                                    4 100.0%
!*VEC estimated potential speedup: 2.000000.                                     2  50.0%
!*VEC loop was vectorized (no peel/no remainder)                                 2  50.0%
!*VEC scalar loop cost: 3.                                                       1  25.0%
!*VEC vector loop cost: 1.500000.                                                1  25.0%
!*VEC loop inside vectorized loop at nesting level: 2.                           1  25.0%
!*VEC LOOP WAS VECTORIZED.                                                       1  25.0%
!*VEC unmasked aligned streaming stores: 1.                                      1  25.0%
!*VEC unmasked aligned streaming stores: 3.                                      1  25.0%
!*VEC unmasked aligned unit stride stores: 2.                                    1  25.0%
!*VEC medium-overhead vector operations: 6.                                      1  25.0%
!*VEC scalar loop cost: 9.                                                       1  25.0%
!*VEC FUSED LOOP WAS VECTORIZED.                                                 1  25.0%
!*VEC medium-overhead vector operations: 18.                                     1  25.0%
!*VEC loop inside vectorized loop at nesting level: 1.                           1  25.0%
!*VEC unmasked aligned unit stride stores: 6.                                    1  25.0%
!*VEC vector loop cost: 4.500000.                                                1  25.0%
!*VEC Total Source Locations:                                                    4
program main

include 'SIZE'

real*8 g(n2,d),x(n2),y(n2),z(n2)

!*VEC (col. 1) LOOP WAS VECTORIZED.
!*VEC (col. 1) estimated potential speedup: 2.000000.
!*VEC (col. 1) loop inside vectorized loop at nesting level: 2.
!*VEC (col. 1) loop was vectorized (no peel/no remainder)
!*VEC (col. 1) medium-overhead vector operations: 6.
!*VEC (col. 1) scalar loop cost: 3.
!*VEC (col. 1) streaming store was generated for main.
!*VEC (col. 1) unmasked aligned streaming stores: 1.
!*VEC (col. 1) unmasked aligned unit stride stores: 2.
!*VEC (col. 1) vector loop cost: 1.500000.
g=1

!*VEC (col. 1) FUSED LOOP WAS VECTORIZED.
!*VEC (col. 1) estimated potential speedup: 2.000000.
!*VEC (col. 1) loop inside vectorized loop at nesting level: 1.
!*VEC (col. 1) loop was vectorized (no peel/no remainder)
!*VEC (col. 1) medium-overhead vector operations: 18.
!*VEC (col. 1) scalar loop cost: 9.
!*VEC (col. 1) streaming store was generated for main.
!*VEC (col. 1) unmasked aligned streaming stores: 3.
!*VEC (col. 1) unmasked aligned unit stride stores: 6.
!*VEC (col. 1) vector loop cost: 4.500000.
x=5
!*VEC (col. 1) streaming store was generated for main.
y=6
!*VEC (col. 1) streaming store was generated for main.
z=7

call foo(g,x,y,z)

print *,x(100)

stop
end program main
