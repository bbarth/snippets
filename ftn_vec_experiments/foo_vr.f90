!*VEC foo.f90
!*VEC                                                                    Source Locations
!*VEC Message                                                                 Count     %
!*VEC estimated potential speedup: 2.310000.                                     1 100.0%
!*VEC unmasked aligned unit stride loads: 36.                                    1 100.0%
!*VEC lightweight vector operations: 18.                                         1 100.0%
!*VEC LOOP WAS VECTORIZED.                                                       1 100.0%
!*VEC unmasked aligned unit stride stores: 6.                                    1 100.0%
!*VEC vector loop cost: 28.500000.                                               1 100.0%
!*VEC scalar loop cost: 66.                                                      1 100.0%
!*VEC loop inside vectorized loop at nesting level: 1.                           1 100.0%
!*VEC medium-overhead vector operations: 78.                                     1 100.0%
!*VEC loop was vectorized (no peel/no remainder)                                 1 100.0%
!*VEC Total Source Locations:                                                    1
subroutine foo(g,x,y,z)

include 'SIZE'

real*8 g(n2,d),x(n2),y(n2),z(n2)
real*8 t1,t2,t3

!dir$ vector aligned
!*VEC (col. 1) LOOP WAS VECTORIZED.
!*VEC (col. 1) estimated potential speedup: 2.310000.
!*VEC (col. 1) lightweight vector operations: 18.
!*VEC (col. 1) loop inside vectorized loop at nesting level: 1.
!*VEC (col. 1) loop was vectorized (no peel/no remainder)
!*VEC (col. 1) medium-overhead vector operations: 78.
!*VEC (col. 1) scalar loop cost: 66.
!*VEC (col. 1) unmasked aligned unit stride loads: 36.
!*VEC (col. 1) unmasked aligned unit stride stores: 6.
!*VEC (col. 1) vector loop cost: 28.500000.
do i=1,n2

   t1=g(i,1)*x(i) + g(i,2)*y(i) + g(i,3)*z(i)
   t2=g(i,2)*x(i) + g(i,4)*y(i) + g(i,5)*z(i)
   t3=g(i,3)*x(i) + g(i,5)*y(i) + g(i,6)*z(i)

   x(i)=t1
   y(i)=t2
   z(i)=t3

enddo

return
end

