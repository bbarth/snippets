clear all
close all

n=100;
d=10;
small = 1e-3;

neigs=floor(rand(n,1)*floor(n/3))+1;
N=sum(neigs);

A=[];

for i=neigs'
  l=rand(1,1)+small;
  B=diag(ones(i,1)*l)+diag(ones(i-1,1),1);
  A=blkdiag(A,B);
end

b=rand(N,1);

x1=A\b;
x2=bicgstab(A,b,1e-10,N);

C=A+small*diag(rand(N,1));

x3=C\b;

norm(x1-x3,2)/norm(x3,2)
x4=bicgstab(A,b,1e-10,1000);
