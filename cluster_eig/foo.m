clear all
close all

n=10;
N=1000;
small=1.e-5;

l=rand(n,1);

A=zeros(N,N);

for i=1:N
  j=n*floor(i/n);
  k=mod(i,n);

  A(j+k,j+k)=l(k+1)+small*rand();

end

plot(real(eig(A)),imag(eig(A)),'x')

B=rand(N,1);

x1=A\b;

x2=pcg(A,b,1e-10,1000);