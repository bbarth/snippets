clear all
close all

n=20;

b=ones(n,1);
epsilon=0.5;

A=epsilon * diag(ones(n,1)) + (diag(2*ones(n,1)) + diag(-ones(n-1,1),1) ...
                            + diag(-ones(n-1,1),-1));


A(1,1)=1;
A(n,n)=1;

b=b-A(:,1).*b;
b=b-A(:,n).*b;

A(:,1)=zeros(size(A(:,1)));
A(:,n)=zeros(size(A(:,n)));

A(1,:)=zeros(size(A(1,:)));
A(n,:)=zeros(size(A(n,:)));

A(1,1)=1;
A(n,n)=1;

%plot(real(eig(A)),imag(eig(A)),'x');

u=A\b;
plot(linspace(0,1,n),u);


