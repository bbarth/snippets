clear all
close all

V=1;
L=2.1;

% c=L-a
% b \in R \implies a <= L
% a > L/2 - sqrt(L^2/4-V)
% a < L/2 + sqrt(L^2/4-V)
% a>0 (pos def A)

D=L/2 - sqrt(L^2/4-V) 
E=L/2 + sqrt(L^2/4-V)
amax=min(L,E);
amin=max(0.,D);
da=(amax-amin)/100;
a=[amin+da:da:amax-da];
c=L-a;
b=sqrt(-V + L^2/4 - (a-L/2).^2) 

%for i = 1:length(a)
%  A=[a(i) b(i) ; b(i) c(i)];
%  det(A)
%  trace(A)
%end