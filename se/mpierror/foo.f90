program main
    implicit none 
    include 'mpif.h'

    integer rank, size, ierr

    call MPI_INIT( ierr )
    call MPI_COMM_RANK( MPI_COMM_WORLD, rank, ierr )
    call MPI_COMM_SIZE( MPI_COMM_WORLD, size, ierr )
    ! print *, 'Process ', rank, ' of ', size, ' is alive' 

    if (rank .eq. 0) then
        call sender()
    end if
    if (rank .eq. 1) then
        call recver()
    end if
    call MPI_FINALIZE( ierr )
end program

subroutine sender()
    implicit none
    include 'mpif.h'

    integer i, ierr
    integer(kind=8) dat(8)
    do i=1, 8
        dat(i) = i
    enddo
    call MPI_SEND( dat, 8, MPI_INTEGER4, 1, 2001, MPI_COMM_WORLD, ierr )
    print *, "Sent     ", dat

end subroutine

subroutine recver()
    implicit none
    include 'mpif.h'

    integer i, ierr
    integer stat
    integer*8 dat(8)

    dat = -1
!    print *, "Empty    ", dat
    call MPI_RECV( dat, 8, MPI_INTEGER4, 0, 2001, MPI_COMM_WORLD, stat, ierr )
    print *, "Received ", dat

end subroutine

