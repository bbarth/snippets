#ifndef BAR_H
#define BAR_H

#ifndef M_PI
#define M_PI 3.141592653
#endif

void dropsym(void *addr);
void dropsym_by_name(char *name);
double bar(double x);

double bills_cos(double x);

#endif

