#define _GNU_SOURCE
#include <dlfcn.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "bar.h"
#include "tacc_timers.h"

extern int max_total_iters;

// why does this have to be [] instead of pointers? question for later
extern unsigned long starts[];
extern unsigned long ends[];

extern int loc;

double bar(double x)
{
  int inner_iterations=0;
  int outer_iterations=0;
  int chip=tacc_get_socket_number();
  int core=tacc_get_core_number();
  int error;

  double ret=0;

  char *a=getenv("NUM_INNER_ITERS");

  if(a)
    inner_iterations=atol(a);
  else
    inner_iterations=1000;

  char *b=getenv("NUM_OUTER_ITERS");
  if(b)
    outer_iterations=atol(b);
  else
    outer_iterations=1000;

  printf("INNER: %d, OUTER: %d\n",inner_iterations,outer_iterations);

  /* 1. drop into an big outer loop NN=getenv(NUM_OUTER_ITERS) to time a spinner thread on a function from a shared library.
     2. After N=getenv(NUM_ITERS), global, change the resolution of the function to a different function that sleeps less or does different work.

   */

  //  double ret=0.;

  int total_count=0;

  // Do half the iterations of cos(x) then switch functions. Abort if total function count goes over MAX_TOTAL_ITERS
  for (int outer=0; outer<outer_iterations/2;outer++)
    {
      loc=0;
      unsigned r=tacc_rdtscp(&chip,&core);
      starts[loc]=r;

      for(int inner=0; inner<inner_iterations; inner++)
        {
          total_count++;
          ret=cos(x);
          if (total_count>max_total_iters) { printf("too many iterations: %d!\n", total_count); goto done;}
        }
      if (total_count>max_total_iters) { printf("too many iterations: %d!\n", total_count); goto done;}
    }
 done:
      ends[loc]=tacc_rdtscp(&chip,&core);
      chip=tacc_get_socket_number();
      core=tacc_get_core_number();
      loc=1;
      starts[loc]=tacc_rdtscp(&chip,&core);
      chip=tacc_get_socket_number();
      core=tacc_get_core_number();
      ret=cos(x);
      total_count++;
      ends[loc]=tacc_rdtscp(&chip,&core);
      chip=core=0; // probably can remove this in favor of the above calls
      chip=core=0; // ask JDM

      dropsym_by_name("cos");
      return(ret);
}

double bills_cos(double x)
{
  // Call to the OG cos() for the moment, but will probably put x^2 and
  // nanosleep() or something
  return cos(x);
}

void dropsym(void *addr)
{
  /* 1. dlopen sym or dladdr the function pointer address (linux/Glibc-specific)
     2. dlsym sym w/ RTLD_DEFAULT or _NEXT pseudo-handle to find another handle
     3. See what happens at the next call

     Do I have dlclose on the handle on sym before any of this first?
  */

  Dl_info dli;
  void *error;
  dlerror();
  printf("before: %p\n",addr);
  int r=dladdr(addr, &dli);
  printf("after: %p\n",addr);

  
  if ((error = dlerror()) != NULL)  {
    fprintf(stderr, "%s\n", error);
    exit(EXIT_FAILURE);
  }
  printf("Got back: %s, %p, %s, %p\n", dli.dli_fname,dli.dli_fbase,dli.dli_sname,dli.dli_saddr);
  
}

void dropsym_by_name(char *name)
{
  
  Dl_info dli;
  void *handle=RTLD_DEFAULT;
  dlerror();

  handle=dlsym(RTLD_DEFAULT,name);

  if ((handle = dlerror()) != NULL)  {
    fprintf(stderr, "%s\n", handle);
    exit(EXIT_FAILURE);
  }

  printf("in by name: %s, %p\n",name, handle);
  
  if ((handle = dlerror()) != NULL)  {
    fprintf(stderr, "%s\n", handle);
    exit(EXIT_FAILURE);
  }
  printf("big diff: %d\n",handle-RTLD_DEFAULT);
  dropsym(cos); //putting an explicit dropsym(cos) here does something...

}
