#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include "bar.h"
#include "tacc_timers.h"

int max_total_iters=100000;
unsigned long starts[2];
unsigned long ends[2];
int loc=0;

int main(int argc, char* argv[])
{
  float tscf=get_TSC_frequency();
  double x=0.;
  starts[0]=0;
  starts[1]=0;
  ends[0]=0;
  ends[1]=0;
  
  char *c=getenv("MAX_TOTAL_ITERS");
  if(c)
    max_total_iters=atol(c);
  
  for (int i=0; i<max_total_iters; i++)
    {
      // McCalpin Timer Start
      //      starts[loc]=tacc_rdtscp(&zero,&zero);
      x=bar(5.*M_PI); // doubly nested main driver in shared library
      //      ends[loc]=tacc_rdtscp(&zero,&zero);
      // McCalpin Timer End
      unsigned long delta=ends[loc]-starts[loc];
      double fdelta=((double)(delta))/tscf;
      printf("rdtscp diff 0/0: %llu\n",delta);
      printf("got: %15.7g, delta: %15.7g, clock: %15.7g\n",x, fdelta,tscf);

      // Need a print in here of the loc=1 deltas
    }

  
  return 0;
}
