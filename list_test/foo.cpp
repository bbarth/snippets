#include <list>
#include <iostream>

using namespace std;

int main()
{
  list<int> a;

  a.push_back(1);
  a.push_back(2);
  a.push_back(3);
  a.push_back(4);
  a.push_back(5);

  for(list<int>::iterator it=a.begin(); it!=a.end(); ++it)
    for(list<int>::iterator jt=it; ++jt!=a.end(); )
      if( (*it + *jt) == 6)
        a.erase(jt);

  for(list<int>::iterator it=a.begin(); it!=a.end(); ++it)
     cout << *it << endl;
 
   return(0);
  
}

