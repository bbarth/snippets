#!/usr/bin/env python

import multiprocessing
import random
import time
import logging

def do_work(x):
  logger=logging.getLogger(__name__)
  logger.setLevel(logging.DEBUG)
  logger.logProcesses=0
  lh=logging.StreamHandler() # put stderr here?
  lh.setLevel(logging.DEBUG)
  fm=logging.Formatter('%(message)s')
  lh.setFormatter(fm)
  logger.addHandler(lh)
  time.sleep(x)
  try:
    a=1/0
  except ZeroDivisionError:
    logger.warning( str(x) )
  return x*x

def main():
  r=[0.1*random.random() for x in range(32)]

  pool=multiprocessing.Pool(processes=32)

  pool.map(do_work,r)

#  pool.close()
#  pool.join()
#
#  print dir(pool)
  

if __name__ == "__main__":
  main()
