#!/usr/bin/env python

import multiprocessing
import random
import time

def do_work(x):
  time.sleep(x)
  return x*x

def main():
  r=[0.1*random.random() for x in range(0)]

  pool=multiprocessing.Pool(processes=32)

  pool.map(do_work,r)

  pool.close()
  pool.join()

  print dir(pool)
  

if __name__ == "__main__":
  main()
