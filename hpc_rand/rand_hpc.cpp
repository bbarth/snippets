/*********************************************
 * *   DESCRIPTION:
 * *   Random Numbers Assignment - C++ Version
 * *   Written for TACC HPC
 * *   Author: Si Liu
 * *   Last update: Mar 25, 2015
 * ***********************************************/

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int Random_N(int Max_N)			
// Generate a random number from 0 to Max_N-1
{
	int res=rand()%Max_N;
	return res;
}

double myaverage(vector<int>::iterator first, vector<int>::iterator end)
// Calculate the average of an int vector
{
	if (first>=end)
		return -1;
	
	int sum=0, count=0;
	vector<int>::iterator it;
	for (it=first;  it!=end; it++)
		{
		sum+=*it;		
		count++;
		}
	return sum*1.0/count;
} 

int main(int Argc, char * Argv[])
{
	vector<string> MyNames;
	vector<int>    MyDutyCounts;
	vector<vector<int> > MyNumbers;

	if (Argc<=1)
	{
		cerr<<"Error! Please provide the List of Names and Counts, and run the program as:\n rand_hpc dutycount"<<endl;
		return -1;
	}

// 1) Read the Name and Count list as input
	ifstream infile(Argv[1]);
	if(!infile)
	{	
		cerr <<" Error! Can not find/open file: "<<Argv[1]<<endl;
		return -2;
	}

	string Lastname, Firstname;
	int Count;
	int TotalDuties=0;

	while(!infile.eof())
	{
	        Count=0;
		infile>>Firstname, infile>>Lastname, infile>>Count;		
		if (Count>0)
		{
			cout<<Firstname<<" "<<Lastname<<" "<<Count<<endl;
			string TempString=Firstname+" "+Lastname;
			MyNames.push_back(TempString); MyDutyCounts.push_back(Count);
			TotalDuties+=Count;
		}
	}
	infile.close();

	int DutyPeople=MyNames.size();
	if (DutyPeople<=5)
	{
		cerr <<" WOW, we need more HPCers to work!" <<endl;
		return -1;
	}	
	MyNumbers.resize(DutyPeople);

//2) Generate random numers!

	srand(time(NULL));
	
	vector<int> Baskets;
	Baskets.resize(TotalDuties);

	cout<<endl<<DutyPeople<<" HPCers randomly choose "<<TotalDuties<<" numbers.\n"<<endl;

	int pos=0;
	for (int i=0; i!=DutyPeople; i++)
		for (int j=0; j!=MyDutyCounts[i]; j++)
			Baskets[pos++]=i;
	
	for (int i=1; i<=TotalDuties; i++)
	{
		int Ball=Random_N(TotalDuties-i+1);
		int Current=0;
		while(1)
		{
			if (Baskets[Current]>=0)  // If Basket is still available:
			{
				if (Ball==0)
				{
					MyNumbers[Baskets[Current]].push_back(i);      //Number "i" is assigned!
					Baskets[Current]=-1;			       //Basket is occupied!
					break;
				}
				Ball--;
			}	
			Current++;
		}
//	cout<<"Number "<<i<<" is assigned to "<<Current<<endl;	
	}	
		
//3) Print the results
	ofstream outfile("result");	
	cout<<"--------------------------------------------------------------------"<<endl;
        outfile<<"--------------------------------------------------------------------"<<endl;
	for (int j=0; j!=MyNames.size(); j++)
	{
		cout << setw(20) <<MyNames[j]<<" got the following numbers: ";
		outfile<<setw(20) <<MyNames[j]<<" got the following numbers: ";
		for (vector<int>::iterator it=MyNumbers[j].begin(); it!= MyNumbers[j].end(); it++)
		{
			cout<<*it<<" ";
			outfile<<*it<<" ";
		}
		cout<<" Ave("<<myaverage(MyNumbers[j].begin(), MyNumbers[j].end())<<")";
		cout<<endl<<"--------------------------------------------------------------------"<<endl;
		outfile<<" Ave("<<myaverage(MyNumbers[j].begin(), MyNumbers[j].end())<<")";
		outfile<<endl<<"--------------------------------------------------------------------"<<endl;
	
	}
	outfile.close();
	cout<<endl<<"Done. The result has been stored into the file result as well."<<endl;	
	return 0;

}
