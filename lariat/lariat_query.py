#!/usr/bin/env python

import json
import time, os, fnmatch, sys, re

def main():

  for directory in sys.argv[1:]:
  
    matches=[]
    
    for root, dirnames, filenames in os.walk(directory):
      for fn in fnmatch.filter(filenames,'*'+'.json'):
        matches.append(os.path.join(root,fn))

    for f in matches:
      ld=json.load(open(f))

      for jobid in ld.keys():
        if ld[jobid]['queue'] is 'gpu':
          runtime=0
          for ibr in ld[jobid]:
            runtime + = ibr['endEpoch']-ibr['startEpoch']

if __name__ == "__main__":
  main()
  
