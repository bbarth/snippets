#!/usr/bin/env python

import json
import time, os, fnmatch, sys

def main():
  user=sys.argv[1]
  directory=sys.argv[2]
  end_epoch=1371148753
  
  matches=[]

  for root, dirnames, filenames in os.walk(directory):
    for fn in fnmatch.filter(filenames,'*'+'.json'):
      matches.append(os.path.join(root,fn))

  runtime=0
  for f in matches:
    ld=json.load(open(f))

    for jobid in ld.keys(): 
      try:
        found_user=ld[jobid][0]['user'];
      except KeyError:
        continue

      if found_user == user:
        for ibr in ld[jobid]:
          start=float(ibr['startEpoch'])
#          end=float(ibr['endEpoch'])
#          diff=end-start
                      
          print jobid+' '+ibr['runTime']+' '+ibr['numCores']+' '+str(start)
          runtime=runtime+(float(ibr['runTime'])*float(ibr['numCores']))
      
  print runtime/3600.
          


if __name__ == "__main__":
  main()
  
