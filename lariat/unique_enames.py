#!/usr/bin/env python

import json
import time, os, fnmatch, sys, re

def comp_name(name,patterns):
  for i in patterns.keys():
    if re.search(i,name):
      return patterns[i]

  return name


def main():

  equiv_patterns = {
    r'^charmrun' : 'Charm++*',
    r'^wrf' : 'WRF*',
    r'^vasp' : 'VASP*',
    r'^run\.cctm' : 'CMAQ CCTM*',
    r'^lmp_' : 'LAMMPS*',
    r'^mdrun' : 'Gromacs*',
    r'^enzo' : 'ENZO*',
    r'^dlpoly' : 'DL_POLY*',
    r'^su3_' : 'MILC*',
    r'^qcprog' : 'QCHEM*',
    r'^namd2' : 'NAMD*',
    r'^cactus' : 'Cactus*',
    r'^pw.x' : 'Quantum Espresso*'
    }
  
  directory=sys.argv[1]
  
  matches=[]

  for root, dirnames, filenames in os.walk(directory):
    for fn in fnmatch.filter(filenames,'*'+'.json'):
      matches.append(os.path.join(root,fn))

  ename={}
  for f in matches:
    ld=json.load(open(f))

    for jobid in ld.keys(): 
      for ibr in ld[jobid]:
        try:
          ename[ibr['exec'].split('/')[-1]]=1
        except KeyError:
          continue

  output={}
  for x in sorted(ename.keys()):
    output[comp_name(x,equiv_patterns)]=1

  for x in sorted(output.keys()):
    print x

if __name__ == "__main__":
  main()
  
