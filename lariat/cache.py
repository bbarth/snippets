#!/usr/bin/env python

import json
import time, os, fnmatch, sys, re

class LariatData:
  def __init__(self,filename,data=None):
    self.ld=None
    if data==None:
      self.ld=json.load(open(filename))
    else:
      self.ld=data.ld.copy()
      self.ld.update(json.load(open(filename)))

def main():

  ld=LariatData(sys.argv[1])
  print ld.ld.keys()
  ld2=LariatData(sys.argv[2],ld)
  print ld2.ld.keys()


if __name__ == "__main__":
  main()
  
