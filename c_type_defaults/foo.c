#include <stdio.h>
#include <stdlib.h>

double foo(int n,double b)
{

  for (int i=0; i < n; ++i)
    b*=13.;

  return b;
}

double bar(int n, double b)
{

  for (int i=0; i < n; ++i)
    b*=13;

  return b;
}

int main()
{
  double b=bar(rand(),(double)rand());
  double f=foo(rand(),(double)rand());
  return (int)(b*f) % 128;
}
  
