#!/bin/bash

for pid in `pgrep $1`; do
  for i in /proc/$pid/task/*; do
    status=$(cat $i/stat | awk '{print $3}')
    if [ $status == "R" ]; then
      echo -n "$i: "
      cat $i/stat | awk '{print $(NF-5)}'
    fi
  done
done


  

