#!/bin/bash

icc -c -o foo_avx2.o foo.c -xAVX2 -axCORE-AVX512 -O3

icc -c -o main_avx2.o main.c -xAVX2 -axCORE-AVX512 -O3

icc -c -o main.o main.c -O3

icc -o main_no main.o foo.o -O3

icc -o main_avx2 main_avx2.o foo.o -O3 -xAVX2 -axCORE-AVX512

