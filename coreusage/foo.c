// Program to change the core size on task 0 to inifinity and force it
// to 0 on everything else. Prevents massive core dumps from big runs.

#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "mpi.h"

int bar(void) // Function which automatically segfaults, ask McLay for
              // details (saves a call to raise(SIGSEGV) and is
              // gdb'able in that you can at least know that this line
              // caused the segmentation fault
{
  return *(int *)0=1;
}

int main(int argc, char* argv[])
{
  int ierr, myProc, nProc;

  ierr = MPI_Init(&argc, &argv);
  
  MPI_Comm_rank(MPI_COMM_WORLD, &myProc);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);

  struct rlimit r; // place where setrlimit and getrlimit store their
                   // limits

  if (myProc == 0)  // If I'm rank 0, set the core size to unlimited
    {
      r.rlim_cur=r.rlim_max=RLIM_INFINITY;
      setrlimit(RLIMIT_CORE,&r);
    }
  else // everybody else ensure that it's zero
    {
      r.rlim_cur=r.rlim_max=0;
      setrlimit(RLIMIT_CORE,&r);
    }

  getrlimit(RLIMIT_CORE,&r);

  printf("core max on %d: %d\n",myProc,r.rlim_max);

  bar(); // Cause a segfault on every process. Should only core dump
         // on task 0, on Linux
  
  MPI_Finalize();
}


