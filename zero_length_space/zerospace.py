#!/bin/sh
# -*- python -*-
# This file is python bilingual: it starts a comment in Python,
# and is a no-op in shell

""":"
# Find a suitable python interpreter 
for cmd in python3 python python2; do
command -v > /dev/null $cmd && exec $cmd $0 "$@"
done

echo "Error: Could not find a valid python interpreter --> exiting!"
exit 2
":"""

from __future__ import print_function
import os, sys, re

# This script prints a message with a unicode zero-length space in it and tries
# to create a file with a zero-length space in the filename. There's no error
# handling, so it will probably throw an error if it's not supported on your
# OS. Inspired by a Facebook conversation with Damien Warman. Preliminaary
# testing (as of the writing of this comment) shows that this all works on HFS
# on OSX (Fri Apr 17 10:06:24 CDT 2020). Further testing to ensue. 

def main():
  print ("Hello World!")
  try:
    s = unicode(u"Zero length space -----\u200B-----").encode('utf8')
  except (ModuleNotFoundError, NameError):
    s = "Zero length space -----\u200B-----"
  print(s)
  fn=u"bad\u200Bfilename.out"
  fh=open(fn,"w")
  fh.write("I am awesome!\n")
  fh.close()
  print ("Goodbye World!")

if ( __name__ == '__main__'): main()

  
  
