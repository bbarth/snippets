#!/usr/bin/env python

from collections import defaultdict

def main():

  start=0
  end=1406869200

  first=defaultdict(lambda : end*2 )
  count=defaultdict(int)
  for l in open('micjobs.log'):

    l=l[:-1]
    (uid,epoch) = l.split(' ');
    epoch=int(epoch)
    if ( (epoch >= start ) and (epoch < end ) ):
      first[uid]=min(first[uid],epoch)
      count[uid]+=1
      
  for (uid,firsttime) in sorted(first.iteritems(), key=lambda x: x[1]):
    print ','.join([uid,str(firsttime),str(count[uid])])

if __name__=='__main__':
  main()
