#include <stdio.h>

#include "mpi.h"

#define N (1024*1024)

int main(int argc, char **argv)
{
  double a[N];
  int p;

  MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE,&p);
  
  #pragma omp parallel for
  for (int i=0; i<N; ++i)
    a[i]=i;

  MPI_Finalize();
}
