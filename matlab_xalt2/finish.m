warning('off','MATLAB:pfileOlderThanMfile'); %% ignore old pfile warning

%% Grab necessary XALT variables

% disp(['In finish.m in ' pwd ])

XD=getenv('XALT_DIR');
XUUID=getenv('XALT_RUN_UUID'); 

lic=license('inuse'); % get used licenses
% Only one is listed (matlab) if no toolboxes are in use;
% Quit if only "matlab" is used
% If XALT_RUN_UUID is unset, also quit
if (~isempty(XUUID)) && (length(lic)>1)

  for i=1:length(lic)
    %% Form basic string for XALT record call, -u is important here
    lic_str=[XD '/libexec/xalt_record_pkg -u ' XUUID ' program matlab xalt_run_uuid ' XUUID ' package_name '];
    version_num=''; % space for version num (used below)
    package_dir=''; % space for path to toolbox (used below)
    a=lic(i).feature;
    if strcmp(a,'matlab') % (shouldn't ever test true due to test above)
      continue
    end
    s=split(a,'_'); % parse off foo_toolbox into foo and toolbox
    v=ver(s{1});    % save foo
    version_num=v.Version; % get the version number
    package_dir=toolboxdir(s{1}); % get the full path
  end

  lic_str=[lic_str a ' package_version ' version_num ' package_path ' ...
           package_dir '> /dev/null 2>&1' ]; % add the version number and path to the
                          % XALT string
%  disp(lic_str) % hide this in finish.m
  system(lic_str); %% uncomment for finish.m and call
                   % xalt_record_pkg with full argument list
else
end
