warning('off','MATLAB:pfileOlderThanMfile'); %% ignore old pfile warning

XD=getenv('XALT_DIR');
XUUID=getenv('XALT_RUN_UUID');

lic=license('inuse'); % get used license, quit if only "matlab" is used
if (~isempty(XUUID)) && (length(lic)>1)

  for i=1:length(lic)
    lic_str=[XD 'libexec/xalt_record_pkg -u ' XUUID ' program matlab xalt_run_uuid ' XUUID ' package_name '];
    version_num='';
    package_dir='';
    a=lic(i).feature;
    if strcmp(a,'matlab')
      continue
    end
    s=split(a,'_');
    v=ver(s{1});
    version_num=v.Version;
    package_dir=toolboxdir(s{1});
  end

  lic_str=[lic_str a ' package_version ' version_num ' package_path ' ...
           package_dir ];
  
  disp(lic_str) % hide this in finish.m
%  system(lic_str) %% uncomment for finish.m
else
end

