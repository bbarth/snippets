#include <stdio.h>

#include "mpi.h"

void init(int *argc, char ***argv)
{
  int provided,np,rank;
  MPI_Init_thread(argc,argv,MPI_THREAD_MULTIPLE,&provided);
  MPI_Comm_size(MPI_COMM_WORLD,&np);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  printf ("%d of %d\n",rank,np);
  
   
}
