clear all
close all

N=100;
n=5;
itermax=1000;



for i=1:itermax
  pos=ceil(rand()*N);
  perm=randperm(n);
  P=zeros(N,n);
  I=eye(n);
  J=I(perm,:);
  P(pos:pos+n-1,:)=J;
  if ~isequal(P'*P,I)
    P
  end
end