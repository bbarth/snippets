clear all
close all

A=[1 -1  0  0  0;
  -1  2  1  0  0;
   0 -1  2  0  0;
   0  0 -1  0  0;
   0  0  0  0  0 ];
B=[0  0  0  0  0;
   0  0  0  0  0;
   0  0  0 -1  0;
   0  0  0  2  1;
   0  0  0 -1  1];

M=A+B;

rnka=rank(A)
rnkb=rank(B)

rbt=orth(B')
na =null(A)
nat=null(A')
rb =orth(B)

Prbt = rbt*rbt'
Pna  = na*na'
Pnat = nat*nat'
Prb  = rb*rb'

Sab=Prbt*Pna
Tab=Pnat*Prb

PIapb=pinv(A+B)
I=eye(size(M));
PIFapb=(I-pinv(Sab))*pinv(A)*(I-pinv(Tab)) + pinv(Sab)*pinv(B)* ...
       pinv(Tab)

